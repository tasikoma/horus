@extends('layouts.homeLayout')
@section('content')

<div class="container mc-auto bg-white shadow-sm">

    <h1>Edit My Profile</h1>


  <div class="container mc-auto bg-white shadow-sm">
    @include('layouts.errordiv')
    <form  id="savemyprofile" action="{{ route('myprofile.update') }}" method="post">
        {{ method_field('PATCH') }}

      @csrf
    <table class="table .table-striped .table-hover" style="width:100%">
      <tr>
        <td>{{ __('Company ID')}}</td>
        <td> {{ $user->username }} </td>
      </tr>
      <tr>
        <td>{{ __('Full Name')}}</td>
        <td>{{ decrypt($user->fullname) }} </td>
      </tr>
      <tr>
        <td>{{ __('E-mail')}}</td>
        <td>{{ $user->email }} </td>
      </tr>
      <tr>
        <td>{{ __('Joining Date')}}</td>
        <td>{{ decrypt($user->joining_date) }}</td>
      </tr>
      <tr>
        <td>{{ __('Termination Date')}}</td>
        <td> {{ ($user->termination_date !== null ) ? decrypt($user->termination_date) : 'N/A'}} </td>
      </tr>
      <tr>
        <td>{{ __('Tax ID')}}</td>
        <td><input type="text" name="taxId" value="{{ isset($user) ? decrypt($user->tax_id) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('Permanent address')}}</td>
        <?php
          if(isset($user) && $user->perm_address !== null){
            $perm = explode('__' , decrypt($user->perm_address));
          }
        ?>
        <td>
          <input type="text" name="permAddressCtry" required placeholder="Country" value="{{ isset($perm[0]) ? $perm[0] : ''}}"/>
          <input type="text" name="permAddressZip" required placeholder="Zip Code" value="{{ isset($perm[1]) ? $perm[1] : ''}}"/>
          <input type="text" name="permAddressCity" required placeholder="City" value="{{ isset($perm[2]) ? $perm[2] : ''}}"/>
          <input type="text" name="permAddressStreet" required placeholder="Street" value="{{ isset($perm[3]) ? $perm[3] : ''}}"/>
          <input type="text" name="permAddressNum" required placeholder="Number" value="{{ isset($perm[4]) ? $perm[4] : ''}}"/>
          <input type="text" name="permAddressStairDoor" placeholder="Stairs and Door" value="{{ isset($perm[5]) ? $perm[5] : ''}}"/>
        </td>
      </tr>
      <tr>
        <td>{{ __('Temporary Address')}}</td>
        <?php
          if(isset($user) && $user->temp_address !== null){
            $temp = explode('__' , decrypt($user->temp_address));
          }
        ?>
        <td>
          <input type="text" name="tempAddressCtry"  placeholder="Country" value="{{ isset($temp[0]) ? $temp[0] : ''}}"/>
          <input type="text" name="tempAddressZip"  placeholder="Zip Code" value="{{ isset($temp[1]) ? $temp[1] : ''}}"/>
          <input type="text" name="tempAddressCity"  placeholder="City" value="{{ isset($temp[2]) ? $temp[2] : ''}}"/>
          <input type="text" name="tempAddressStreet"  placeholder="Street" value="{{ isset($temp[3]) ? $temp[3] : ''}}"/>
          <input type="text" name="tempAddressNum"  placeholder="Number" value="{{ isset($temp[4]) ? $temp[4] : ''}}"/>
          <input type="text" name="tempAddressStairDoor" placeholder="Stairs and Door" value="{{ isset($temp[5]) ? $temp[5] : ''}}"/>
        </td>
      </tr>
      <tr>
        <td>{{ __('Unofficial Temporary Address')}}</td>
        <?php
          if(isset($user) && $user->temp_unoff_address !== null){
            $tempUO = explode('__' , decrypt($user->temp_unoff_address));
          }
        ?>
        <td>
          <input type="text" name="tempUOAddressCtry"  placeholder="Country" value="{{ isset($tempUO[0]) ? $tempUO[0] : ''}}"/>
          <input type="text" name="tempUOAddressZip"  placeholder="Zip Code" value="{{ isset($tempUO[1]) ? $tempUO[1] : ''}}"/>
          <input type="text" name="tempUOAddressCity"  placeholder="City" value="{{ isset($tempUO[2]) ? $tempUO[2] : ''}}"/>
          <input type="text" name="tempUOAddressStreet"  placeholder="Street" value="{{ isset($tempUO[3]) ? $tempUO[3] : ''}}"/>
          <input type="text" name="tempUOAddressNum"  placeholder="Number" value="{{ isset($tempUO[4]) ? $tempUO[4] : ''}}"/>
          <input type="text" name="tempUOAddressStairDoor" placeholder="Stairs and Door" value="{{ isset($tempUO[5]) ? $tempUO[5] : ''}}"/>
        </td>
      </tr>
      <tr>
        <td>{{ __('Commuting Daily from')}}</td>
        <td>
            <select class="" name="selectCommuteFrom">
              <option value="perm" {{ $user->daily_commute_from == 'perm' ? 'selected' : ''}} >
                {{ __('Permanent Address')}}
              </option>
              <option value="temp" {{ $user->daily_commute_from == 'temp' ? 'selected' : ''}}>
                {{ __('Temporary Address')}}
              </option>
              <option value="tempUO" {{ $user->daily_commute_from == 'tempUO' ? 'selected' : ''}}>
                {{ __('Temporary Address (unofficcial)')}}
              </option>
            </select>
        </td>
      </tr>

      <tr>
        <td>{{ __('Car Commute Allowed')}}</td>
        <td>{{ $user->car_allowed == 1 ? 'Yes' : 'No' }}</td>
      </tr>
      @if($user->car_allowed == 1)
      <tr>
        <td>{{ __('Car Allowance')}}</td>
        <td>{{ $user->car_allowance_limit >= 1 ? $user->car_allowance_limit : 'Not limited' }}</td>
      </tr>
      @endif
      <tr>
        <td>{{ __('License Plate')}}</td>
        <td><input type="text" name="licensePlate" value="{{ $user->licence_plate != null ? decrypt($user->licence_plate) : '' }}" /></td>
      </tr>
      <tr>
        <td>{{ __('Preferred Language')}}</td>
        <td><select class="" name="selectPreferredLang">
              <option value="en" {{ isset($user) && $user->preferred_lang == 'en' ? 'selected' : ''}}>English</option>
              <option value="hu" {{ isset($user) && $user->preferred_lang == 'hu' ? 'selected' : ''}}>Magyar</option>
            </select>
        </td>
      </tr>
    </table>
    <button class="btn btn-primary" type="submit" name="button">Save</button>
  </form>

  </div>

</div>

@endsection
