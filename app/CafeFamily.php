<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CafeFamily extends Model
{
  protected $guarded = ['id'];

  public $timestamps = false;

  public function children()
  {
      return $this->hasMany('App\CafeElement', 'family_id', 'id');
  }
}
