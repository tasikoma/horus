<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CutoffDates extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
      'year','month','start','end'
    ];
}
