//https://kode-blog.io/laravel-5-ajax-tutorial

function calculateRefund(){
	 gross = document.getElementById('grossAmount_id').value;
		reimb = gross * 0.86 ;
    //console.log(reimb);
		document.getElementById('calculatedrefund').innerHTML= reimb;

}

function carDaylist(){
  var comonth = $('#carMonthSwitch_id :selected').val() ;
  var form_data = new FormData();
  form_data.append('comonth', comonth);
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  $.ajax({
  url: "proc/draw_car_daylist.php",
  type: "POST",
  contentType: false,
  processData: false,
  data: form_data,
  success: function(data){
    $('#dateListTable_id').html('');
    $('#dateListTable_id').html(data);
  }
  });

}

function traveltypereroute(){
	var travelType = $('#selectTT_id :selected').val();
	if(travelType == 'car'){
		window.location = '/tes/mydc/create/car' ;
	}
	else{
		window.location = '/tes/mydc/create' ;
	}
}

function calcSum(){
  var p = $('.expTable');
  var sum = 0;
  var item = 0;
  p.each(function(){
    $(this).children('tbody').find('.refund_cell').each(function(){
      var value = parseInt($(this).text());
      if (!isNaN(value)) {
        sum += value;
      }
      item++;
    });
    $(this).children('tfoot').find('.sum_items_cell').text(sum);
    $(this).children('tfoot').find('.num_items_cell').text(item);
    sum = 0;
    item = 0;
  });
}




$(document).ready(function(){
  $('.carDayCheckboxTD').on('click',function(){
    $(this).toggleClass('highlighted');
    var inp = $(this).find('.carDayCheckbox')
    if(inp.attr('checked')){
      inp.attr('checked', false);
    }
    else{
      inp.attr('checked', true);
    }
  });

  $('.tableshrink').on('click',function(){
    $(this).siblings().toggleClass('hidden');
    $(this).parent().siblings('tbody').toggleClass('hidden');
  });

  //calc individual expense reimbursement

  //summarize table items huf
  calcSum();

	$('.docview').on('click',function(){
		var id = $(this).data('id');
		var src = $(this).data('src');
		var form_data = new FormData();
		form_data.append('id', id);
		form_data.append('src', src);
		$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      }
	    });
	  $.ajax({
		  url: '/tes/admin/filter/docview/',
		  type: "POST",
			processData: false,
      contentType: false,
		  data: form_data,
		  success: function(data){
				var doc = btoa(data);
				//console.log(data);
		    $('.docDisplay').html('<img src="<img src="data:image/jpeg;base64,' + doc + '" />');

		  },
			error: function(e){
				console.log(e);
				$('.docDisplay').html(e);
			}
	  });
	});


	//cafeteris stuff
	$('.cafeAllowedMonthTD').on('click',function(){
    $(this).toggleClass('highlightedMonth');
    var inp = $(this).find('.cafeAllowedMonthCB')
    if(inp.attr('checked')){
      inp.attr('checked', false);
    }
    else{
      inp.attr('checked', true);
			if(inp.val() == 'all'){
				//uncheck the others
			}
    }
  });
});

// ajax stuff for doc display
