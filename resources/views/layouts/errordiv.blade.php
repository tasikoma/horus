<div class="error ">
  @if ($errors->any())
  <div class="alert alert-danger container mc-auto shadow-sm">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
</div>
