<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Approve/Reject Expense</h4>
      </div>
      <div class="modal-body">
        <form class="" action="index.html" method="post">
          @csrf
          <button type="button" name="button">Approve</button>
          <button type="button" name="button">Reject</button>
          <input type="textbox" name="rejReason" value="" placeholder="Reason of rejection">
        </form>
        <div class="docDisplay">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
