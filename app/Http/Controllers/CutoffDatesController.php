<?php

namespace App\Http\Controllers;

use App\CutoffDates;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\ValidateCutoffDate;

class CutoffDatesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('cutoff', ['only' => 'store']);
        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cutoffDates = DB::table('cutoff_dates')->orderBy('start','desc')->get();
        return view('tes.cutoffdatemng', compact('cutoffDates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        CutoffDates::create([
          'year' => intval(date('Y', strtotime($request['coEnd']))) ,
          'month' => date('F', strtotime($request['coEnd'])),
          'start' => $request['coStart'],
          'end' => $request['coEnd']
        ]);

        return redirect('cutoffdates');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CutoffDates  $cutoffDates
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //dd($cutoffDates);
        \App\CutoffDates::destroy($id);
        return redirect('cutoffdates');
    }
}
