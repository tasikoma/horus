<?php

namespace App\Http\Controllers;

use App\CafePlan;
use Illuminate\Http\Request;

class CafePlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cafe.myplan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CafePlan  $cafePlan
     * @return \Illuminate\Http\Response
     */
    public function show(CafePlan $cafePlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CafePlan  $cafePlan
     * @return \Illuminate\Http\Response
     */
    public function edit(CafePlan $cafePlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CafePlan  $cafePlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CafePlan $cafePlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CafePlan  $cafePlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(CafePlan $cafePlan)
    {
        //
    }
}
