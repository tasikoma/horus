@extends('layouts.homeLayout')
@section('content')

<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1>Travel expense management</h1>
  </div>
  <hr>
    @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm filterOpts">
    <table class="table .table-striped .table-hover" style="width:100%">
      <thead>
        <tr>
          <th colspan="2">Search for username:</th>
          <th>Cutoff Period</th>
          <th>Expense Type</th>
          <th>Travel Type</th>
          <th>Ticket Type</th>
          <th>Expense Status</th>
        </tr>
      </thead>
      <form class="" action="{{ route('admin.tes.filter') }}" method="post">
        @csrf
      <tbody>
          <tr>
            <td colspan="2">
              <input type="text" name="fltrUser" placeholder="username">
            </td>
            <td>
              <select class="" name="fltrMonth" >
                <option value="all">{{__('All')}}</option>
                <option value="{{ $months[0]['year'].'-'.$months[0]['month'] }}" selected>{{ $months[0]['year'].'-'.$months[0]['month'] }}</option>
                <option value="{{ $months[0]['year'].'-'.$months[0]['month'] }}">{{ $months[1]['year'].'-'.$months[1]['month'] }}</option>
                <option value="{{ $months[0]['year'].'-'.$months[0]['month'] }}">{{ $months[2]['year'].'-'.$months[2]['month'] }}</option>
                <option value="{{ $months[0]['year'].'-'.$months[0]['month'] }}">{{ $months[3]['year'].'-'.$months[3]['month'] }}</option>
                <option value="{{ $months[0]['year'].'-'.$months[0]['month'] }}">{{ $months[4]['year'].'-'.$months[4]['month'] }}</option>
                <option value="{{ $months[0]['year'].'-'.$months[0]['month'] }}">{{ $months[5]['year'].'-'.$months[5]['month'] }}</option>
              </select>
            </td>
            <td>
              <select class="" name="fltrExpType">
                <option value="all">{{__('All')}}</option>
                <option value="ht">{{__('Home Travel')}}</option>
                <option value="dc">{{__('Daily Commute')}}</option>
              </select>
            </td>
            <td>
              <select class="" name="fltrTravelType">
                <option value="all">{{__('All')}}</option>
                <option value="public">{{__('Public')}}</option>
                <option value="car">{{__('Car')}}</option>
              </select>
            </td>
            <td>
              <select class="" name="fltrTicketType">
                <option value="all">{{__('All')}}</option>
                <option value="ticket">{{__('Ticket')}}</option>
                <option value="pass">{{__('Monthly Pass')}}</option>
                <option value="planeticket">{{__('Plane Ticket')}}</option>
              </select>
            </td>
            <td>
              <select class="" name="fltrExpStatus">
                <option value="all">{{__('All')}}</option>
                <option value="0">{{__('Pending')}}</option>
                <option value="1">{{__('Approved')}}</option>
                <option value="9">{{__('Rejected')}}</option>
              </select>
            </td>
            <td><button class="btn btn-primary" type="submit" name="runFilter">Search</button> </td>
          </tr>
    </tbody>
    </form>
    </table>
  </div>
  <hr>
  <div class="container mc-auto bg-white shadow-sm">
    @if(isset($welcome))
      <div class="alert alert-info">
        <p>
          Welcome! <br> You can search for expense items here, approve and reject them.
        </p>
      </div>
    @endif

    @if(isset($exps))

      @foreach ($exps as $month => $users)
        @foreach ($users as $user => $expTypes)
          @foreach ($expTypes as $expType => $items)
        <table class="expTable table .table-striped .table-hover" style="width:100%">
          <thead class="thead-dark">
            <tr  class="tableshrink">

              <th colspan="12" >{{ $user.' - '.__($month).' - ' }} {{ $expType == 'ht' ? __('Home Travel') : __('Daily Commute')}}</th>
            </tr>
            <tr class="headerlist hidden">
              <th>{{__('Travel Type')}}</th>
              <th>{{__('Ticket Type')}}</th>
              <th>{{__('Submission Month')}}</th>
              <th>{{__('Travel Date')}}</th>
              <th>{{__('From')}}</th>
              <th>{{__('To')}}</th>
              <th>{{__('Gross Amount')}}</th>
              <th>{{__('Distance')}}</th>
              <th>{{__('Refund')}}</th>
              <th>{{__('Status')}}</th>
              <th>{{__('Docs')}}</th>
              <th>{{__('Actions')}}</th>
            </tr>
          </thead>
        <tbody class="hidden">
            @foreach ($items as $item)
              <tr>
                <td> {{ $item->travel_type == 'public' ? __('Public Transport') : __('Car')}} </td>
                <td>
                  @if($item->ticket_type == 'ticket')
                    {{__('Ticket')}}
                  @elseif($item->ticket_type == 'planeticket')
                    {{__('Plane Ticket')}}
                  @elseif($item->ticket_type == 'pass')
                      {{__('Monthly Pass')}}
                  @else
                    {{__('N/A')}}
                  @endif
                </td>
                <td> {{ __($item->sub_month) }} </td>
                <td> {{ $item->travel_date }} </td>
                @if($item->travel_from == 'office')
                  <td> {{ __('Office, Kassák Lajos u.') }} </td>
                @else
                <td> {{ $userAddr[$user][$item->travel_from][2].', '.  $userAddr[$user][$item->travel_from][3]}} </td>
                @endif
                @if($item->travel_to == 'office')
                  <td> {{ __('Office, Kassák Lajos u.') }} </td>
                @else
                <td> {{ $userAddr[$user][$item->travel_to][2].', '.  $userAddr[$user][$item->travel_to][3]}} </td>
                @endif
                <td> {{ $item->exp_gross_amount }}  </td>
                <td> {{ $item->exp_distance }} </td>
                <td class="refund_cell"> {{ $item->exp_refund }}  </td>
                @if($item->exp_status == 0)
                <td> {{__('Pending')}} </td>
                @elseif($item->exp_status == 2)
                <td> {{__('Approved')}} </td>
                @else
                <td> {{__('Rejected')}} </td>
                @endif
                <td>
                  @if(isset($item->doc_url1))
                  <button class="docview btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#myModal"
                  data-id="{{ $item->id }}" data-src="1">
                    <span class="glyphicon glyphicon-file"></span>
                  </button>
                  @endif
                  @if(isset($item->doc_url2))
                  <button class="docview btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#myModal"
                  data-id="{{ $item->id }}" data-src="2">
                    <span class="glyphicon glyphicon-file"></span>
                  </button>
                  @endif
                </td>
                <td>
                  <button class="btn btn-primary" type="button" name="button">Approve</button>
                  <button class="btn btn-primary" type="button" name="button">Reject</button>
                 </td>
                <td> {{ $item->comment }} </td>
              </tr>
              @endforeach

        </tbody>
        <tfoot class="tfoot-dark">
          <tr>
            <td colspan="4" >{{__('Summary:')}}</td>
            <td colspan="4"><span class="num_items_cell"></span>  {{__(' items')}}</td>
            <td colspan="4"><span class="sum_items_cell"></span> HUF</td>
          </tr>
        </tfoot>
        </table>
          @endforeach
        @endforeach
      @endforeach
    @else
      <div class="alert alert-info">No items found with the given filters...</div>
    @endif
      @include('layouts.docmodal')
  </div>

</div>

@endsection
