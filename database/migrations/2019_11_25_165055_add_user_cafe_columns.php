<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserCafeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cafe_eligible_from')->nullable();
            $table->string('cafe_eligible_until')->nullable();
            $table->string('cafe_yearly_limit')->nullable();
            $table->string('cafe_emp_type')->nullable();
            $table->string('cafe_szep_leisure_pocket')->nullable();
            $table->string('cafe_szep_accom_pocket')->nullable();
            $table->string('cafe_szep_dining_pocket')->nullable();
            $table->boolean('cafe_planning_open')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
              $table->dropColumn('cafe_eligible_from');
              $table->dropColumn('cafe_eligible_until');
              $table->dropColumn('cafe_yearly_limit');
              $table->dropColumn('cafe_emp_type');
              $table->dropColumn('cafe_szep_leisure_pocket');
              $table->dropColumn('cafe_szep_accom_pocket');
              $table->dropColumn('cafe_szep_dining_pocket');
              $table->dropColumn('cafe_planning_open');
        });
    }
}
