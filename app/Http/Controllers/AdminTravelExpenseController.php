<?php

namespace App\Http\Controllers;

use App\TravelExpense;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CutoffDates ;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdminTravelExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //adminvalidator here

    }
    /*

    function summary
    - index
    - search with filters
    - Approve
    - Reject
    - Mass Approve
    - MAss reject

    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //return 6 months of CutoffDates
         $months = \App\CutoffDates::where('start', '<', date('Y-m-d') )
                 ->orderBy('start', 'desc')
                 ->take(6)
                 ->get()->toArray();
        $welcome = true ;
         return view('tes.reports', compact('months','welcome'));
     }

     public function filter(Request $request)
     {
       $months = \App\CutoffDates::where('start', '<', date('Y-m-d') )
               ->orderBy('start', 'desc')
               ->take(6)
               ->get()->toArray();
        // filter and search function
        $qry = [];
        //specific user Else not rquired
        if($request['fltrUser'] != null){
          $qry[] = ['username', '=',$request['fltrUser']] ;
        }
        //month chooser
        if($request['fltrMonth'] !== 'all'){
          $chDate = explode('-',$request['fltrMonth']);
          $year = $chDate[0];
          $month = $chDate[1];
          $co = \App\CutoffDates::where('year', '=', $year )
                ->where('month','=', $month) ->get()->toArray();

          $qry[] = ['sub_date', '>=',$co[0]['start']] ;
          $qry[] = ['sub_date', '<=',$co[0]['end']] ;
        }
        else{
          $qry[] = ['sub_date', '>=',date('Y-m-d',strtotime('-10 months'))] ;
        }
        // filter exp type
        if($request['fltrExpType'] !== 'all'){
          $qry[] = ['exp_type', '=', $request['fltrExpType'] ] ;
        }
        // filter travel type
        if($request['fltrTravelType'] !== 'all'){
          $qry[] = ['travel_type', '=', $request['fltrExpType'] ] ;
        }
        //filter ticket type
        if($request['fltrTicketType'] !== 'all'){
          $qry[] = ['ticket_type', '=', $request['fltrTicketType'] ] ;
        }
        // filter exp status
        if($request['fltrExpStatus'] !== 'all'){
          $qry[] = ['exp_status', '=', $request['fltrExpStatus'] ] ;
        }
        $items = DB::table('travel_expenses')
                ->where($qry)
                ->orderBy('username', 'asc')
                ->orderBy('sub_month', 'desc')
                ->orderBy('exp_type', 'asc')
                ->get()->toArray();
        $exps = [];
        $addrArr = [];
        foreach ($items as $index => $ex) {
          $exps[$ex->sub_month][$ex->username][$ex->exp_type][] = $ex;
        }

        foreach($exps as $month => $users){
          foreach($users as $user => $items){
            $addrArr[] = $user;
          }
        }

        $getAddr = DB::table('users')->select('username', 'perm_address as perm', 'temp_address as temp', 'temp_unoff_address as tempUO')
                    ->whereIn('username', $addrArr)-> get()->toArray();
        //pass trough adresses
        $userAddr = [];
        foreach ($getAddr as $key => $val) {
          $userAddr[$val->username]['perm'] = $val->perm != null ? explode('__',decrypt($val->perm)) : null;
          $userAddr[$val->username]['temp'] = $val->temp != null ? explode('__',decrypt($val->temp)) : null;
          $userAddr[$val->username]['tempUO'] = $val->tempUO != null ? explode('__',decrypt($val->tempUO)): null;
        }

        return view('tes.reports', compact('exps','months', 'userAddr'));
     }
     // show ddocument with modal
     //https://medium.com/@allalmohamedlamine/how-to-serve-images-and-files-privatly-in-laravel-5-7-a4b469f0f706
     public function showdoc(Request $request){

          $item = \App\TravelExpense::find($request['id']);

          if($request['src'] == 1){
            $url = $item->doc_url1 ;
          }
          else{
            $url = $item->doc_url2 ;
          }
          if (!Storage::disk('local')->exists($url)){
            abort(404);
          }
          //$response = file(storage_path('app'.DIRECTORY_SEPARATOR.($url)));
          dd(response()->file(storage_path('app'.DIRECTORY_SEPARATOR.($url))));
          return response()->$response;

     }

     public function approveExp(Request $request){

     }

     public function rejectExp(Request $request){

     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TravelExpense  $travelExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(TravelExpense $travelExpense)
    {
        //
    }
}
