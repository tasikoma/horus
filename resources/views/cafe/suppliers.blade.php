@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1>Supplier Management</h1>
  </div>
  @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      <thead class="thead-dark">
        <tr>
          <th>{{ __('Name') }}</th>
          <th>{{ __('Address') }}</th>
          <th>{{ __('E-mail') }}</th>
          <th>{{ __('Phone') }}</th>
          <th>{{ __('Contact Person') }}</th>
          <th>{{ __('Account') }}</th>
          <th>{{ __('Comments') }}</th>
          <th>{{ __('Actions') }}</th>
        </tr>
      </thead>
      <tbody>
        @foreach($suppliers as $sup)
        <tr>
          <td>{{ $sup->name }}</td>
          <td>{{ $sup->address }}</td>
          <td>{{ $sup->mail }}</td>
          <td>{{ $sup->phone }}</td>
          <td>{{ $sup->contact_person }}</td>
          <td>{{ $sup->account }}</td>
          <td>{{ $sup->comment }}</td>
          <td>
            <a href="{{ route('suppliers.edit', $sup->id) }}">
              <button class="btn btn-primary"   type="button" name="editSupplierButton">
                <span class="glyphicon glyphicon-edit"></span>
              </button>
            </a>
            <form class="" action="{{ route('suppliers.destroy', $sup->id) }}" method="post" style="display:inline;">
              @csrf
              {{ method_field('DELETE') }}
              <button class="btn btn-primary"   type="submit" name="DisableSupplierButton">
                <span class="glyphicon glyphicon-trash"></span>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
      <tfoot class="tfoot-dark">
        <tr>
          <td colspan="8" style="text-align:center;">
            <a href="{{ route('suppliers.create') }}">
              <button class="btn btn-primary"  type="button" name="addSupplierBtn">{{ __('Add New Supplier') }}</button>
            </a>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
@endsection
