<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCafeFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cafe_families', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('name');
          $table->tinyInteger('status');
          $table->string('common_tax')->nullable();
          $table->string('common_limit')->nullable();
          $table->string('overall_limit')->nullable();
          $table->boolean('mandatory')->nullable();
          $table->tinyInteger('num_of_mandatory')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cafe_families');
    }
}
