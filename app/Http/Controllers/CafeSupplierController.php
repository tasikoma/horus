<?php

namespace App\Http\Controllers;

use App\CafeSupplier;
use Illuminate\Http\Request;

class CafeSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = CafeSupplier::where('status',1)->get();
        return view('cafe.suppliers',compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $act = 'new';
        return view('cafe.managesupplier', compact('act'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      request()->validate([
        'supplierName' => 'required|max:255',
        'supplierAddress' => 'max:255',
        //'supplierMail' => 'required|in:public,car',
        //'supplierPhone' => 'required_if:selectTravelType,public|in:ticket,planeticket,pass',
        //'supplierContact' => 'required',
        //'supplierAccount' => 'required_if:selectTravelType,public|in:perm,temp,tempUO,office',
        //'supplierComment' => 'required_if:selectTravelType,public|in:perm,temp,tempUO,office',
      ]);
      CafeSupplier::create([
        'name' => $request['supplierName'],
        'address' => $request['supplierAddress'],
        'mail' => $request['supplierMail'],
        'phone' => $request['supplierPhone'],
        'contact_person' => $request['supplierContact'],
        'account' => $request['supplierAccount'] ,
        'comment' => $request['supplierComment'] ,
        'status' => 1 ,
        'modified_by' => auth()->user()->username ,
      ]);

      return redirect( route('suppliers.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CafeSupplier  $cafeSupplier
     * @return \Illuminate\Http\Response
     */
    public function show(CafeSupplier $supplier)
    {
        return redirect( route('suppliers.index') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CafeSupplier  $cafeSupplier
     * @return \Illuminate\Http\Response
     */
    public function edit(CafeSupplier $supplier)
    {
        $act = 'edit';
        $sup = $supplier;
        return view('cafe.managesupplier', compact('act', 'sup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CafeSupplier  $cafeSupplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CafeSupplier $supplier)
    {
        $sup = CafeSupplier::find($supplier)->first();
        $sup -> name = $request['supplierName'];
        $sup -> address = $request['supplierAddress'];
        $sup -> mail = $request['supplierMail'];
        $sup -> phone = $request['supplierPhone'];
        $sup -> contact_person = $request['supplierContact'];
        $sup -> account = $request['supplierAccount'] ;
        $sup -> comment = $request['supplierComment'] ;
        $sup -> modified_by = auth()->user()->username ;
        $sup-> save();

        return redirect( route('suppliers.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CafeSupplier  $cafeSupplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(CafeSupplier $supplier)
    {
        $sup = CafeSupplier::find($supplier)->first();
        $sup -> status = 0 ;
        $sup -> modified_by = auth()->user()->username ;
        $sup-> save();

        return redirect( route('suppliers.index') );
    }
}
