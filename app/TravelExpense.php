<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelExpense extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'username','sub_month','exp_type', 'sub_date', 'exp_status',
        'travel_type', 'ticket_type', 'travel_date', 'travel_from',
        'travel_to', 'exp_gross_amount', 'exp_distance', 'exp_refund',
        'doc_url1', 'doc_url2',
        'reviewed_by', 'comment'
    ];

    
}
