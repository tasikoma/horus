<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cafe', 'HomeController@cafe')->name('cafe');


Route::resource('/userman','UserController');
Route::post('/userman/search','UserController@search')->name('userman.search');

Route::get('/myprofile','MyProfileController@index')->name('myprofile');
Route::get('/myprofile/edit','MyProfileController@edit')->name('myprofile.edit');
Route::patch('/myprofile/update','MyProfileController@update')->name('myprofile.update');

Route::get('/tes/myht', 'TreavelExpenseController@myht')->name('tes.myht');
Route::get('/tes/mydc', 'TreavelExpenseController@mydc')->name('tes.mydc');
Route::get('/tes/myht/create', 'TreavelExpenseController@create')->name('tes.myht.create');
Route::get('/tes/mydc/create', 'TreavelExpenseController@create')->name('tes.mydc.create');
//Route::post('/tes/store', 'TreavelExpenseController@store')->name('tes.store');
//Route::delete('/tes/destroy', 'TreavelExpenseController@destroy')->name('tes.destroy');
Route::get('/tes/mydc/create/car', 'TreavelExpenseController@mycarCreate')->name('tes.mydc.create.mycarcreatecurrent');
Route::post('/tes/mydc/create/car', 'TreavelExpenseController@mycarCreateCust')->name('tes.mydc.create.mycarcreatecustom');
Route::post('/tes/mydc/create/car/store', 'TreavelExpenseController@carexpstore')->name('tes.mydc.carexpstore');


Route::get('/tes/admin','AdminTravelExpenseController@index')->name('admin.tes.index');
Route::post('/tes/admin/filter','AdminTravelExpenseController@filter')->name('admin.tes.filter');
Route::post('/tes/admin/filter/docview/', 'AdminTravelExpenseController@showdoc')->name('admin.tes.displayDoc');

Route::resource('/tes','TreavelExpenseController') ;
Route::resource('/cutoffdates','CutoffDatesController');

//cafe routes
Route::resource('/cafe/suppliers','CafeSupplierController') ;
Route::resource('/cafe/families','CafeFamilyController') ;
Route::resource('/cafe/elements','CafeElementController') ;
Route::resource('/cafe/plan','CafePlanController') ;
