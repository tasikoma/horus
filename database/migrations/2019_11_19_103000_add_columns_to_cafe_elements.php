<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCafeElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cafe_elements', function (Blueprint $table) {
            $table->unsignedBigInteger('family_id')->nullable()->after('name');
            $table->string('multiplier')->nullable()->after('monthly_limit');
            $table->tinyInteger('limit_breachable')->nullable()->after('monthly_limit');
            $table->string('allowed_months')->nullable()->after('needs_fallback');
            $table->string('fund_source')->nullable()->after('needs_fallback');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cafe_elements', function (Blueprint $table) {
            $table->dropColumn('family_id');
            $table->dropColumn('limit_breachable');
            $table->dropColumn('multiplier');
            $table->dropColumn('allowed_months');
            $table->dropColumn('fund_source');
        });
    }
}
