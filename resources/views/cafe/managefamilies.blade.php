@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    @if($act == 'new')
      <h1>{{ __('Add New Cafeteria Element') }}</h1>
    @else
      <h1>{{ __('Edit Cafeteria Element') }}</h1>
    @endif
  </div>
  @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      @if($act == 'new')
      <form class="" action="{{ route('families.store')}}" method="post">
      @else
      <form class="" action="{{ route('families.update', $family->id)}}" method="post">
        {{ method_field('PATCH') }}
      @endif
        @csrf
        <tr>
          <td>{{ __('Family Name')}}</td>
          <td>
            <input type="text" name="familyName" value="{{ isset($family) ? $family->name : '' }}"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Common Tax')}}</td>
          <td>
            <input type="text" name="familyCommonTax" value="{{ isset($family) ? $family->common_tax : '' }}"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Common Limit')}}</td>
          <td>
            <input type="text" name="familyCommonLimit" value="{{ isset($family) ? $family->common_limit : '' }}"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Overall Limit')}}</td>
          <td>
            <input type="text" name="familyOverallLimit" value="{{ isset($family) ? $family->overall_limit : '' }}"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Has manadtory element?')}}</td>
          <td>
            <input type="checkbox" name="familyMandatory" value="1"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Number of mandatory elements')}}</td>
          <td>
            <input type="number" name="familyMandatoryNum" value="{{ isset($family) ? $family->num_of_mandatory : '' }}"/>
          </td>
        </tr>
        <tr>
          <td colspan="2"style="text-align:center;">{{ __('Elements in Family:')}}</td>
        </tr>
        @if(isset($elements))
          @foreach($elements as $el)
            <tr>
              <td colspan="2" style="background:#dcf2e2;text-align:center;">
                @if(isset($family))
                <input type="checkbox" name="addElement_{{ $el->id }}" value="{{ $el->id }}"
                {{ isset($family) && $el->family_id == $family->id ? 'checked ' : ''  }}
                {{ isset($family) && ($el->family_id != $family->id && $el->family_id != null) ? 'disabled ' : '' }}/>
                @else
                <input type="checkbox" name="addElement_{{ $el->id }}" value="{{ $el->id }}" {{ $el->family_id != null ? 'disabled ' : '' }}/>
                @endif
                {{ $el->name }}
              </td>
            </tr>
          @endforeach
        @endif
        <tr>
          <td colspan="2">
            <button class="btn btn-primary" type="submit" name="saveFamily">Save</button>
            <button class="btn btn-primary" type="button" onclick="window.location='{{ URL::previous() }}'" name="cancelManageFamily">Cancel</button>
          </td>
        </tr>
      </form>
    </table>
  </div>
@endsection
