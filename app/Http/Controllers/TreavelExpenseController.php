<?php

namespace App\Http\Controllers;

use App\TravelExpense;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CutoffDates ;
use Illuminate\Support\Facades\Storage;

class TreavelExpenseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('expvalid') -> only('store','carexpstore');

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(strpos(url()->current(),'myht' ) !== false){
          return view('tes.createexp', ['expType' => 'ht']);
        }
        elseif(strpos(url()->current(),'mydc' ) !== false){
          return view('tes.createexp', ['expType' => 'dc']);
        }
        else{
          return view('tes.createexp', ['expType' => 'ht']);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);

        request()->validate([
          'docs.*' => 'required|max:1024',
          'expType' => 'required|in:ht,dc',
          'selectTravelType' => 'required|in:public,car',
          'selectTicketType' => 'required_if:selectTravelType,public|in:ticket,planeticket,pass',
          'dateOfTravel' => 'required',
          'selectFrom' => 'required_if:selectTravelType,public|in:perm,temp,tempUO,office',
          'selectTo' => 'required_if:selectTravelType,public|in:perm,temp,tempUO,office',
          'grossAmount' => 'required_if:selectTravelType,public|numeric|min:1',
        ]);
        //rename files , move them, add url
        //dd($request);
        if ($docs = $request->file('docs')) {
            $i = 0 ;
            foreach ($docs as $doc) {
            $destinationPath = 'tes'; // upload path
            $uplDoc = date('YmdHis') .$i. "." . $doc->getClientOriginalExtension();
            $path = Storage::disk('local')->putFile('tes', $doc);
            $insert[]['doc'] = "$path";
            $i++;
            }
        }
        $prt = session()->get('partial') ;
        $alert = session()->get('alerts');
        if($prt != null){
          $expRefund = $prt;
        }
        else{
          $expRefund = $request['grossAmount'] * 0.86 ;
        }
        $codt = \App\CutoffDates::where('start', '<', date('Y-m-d') )
                ->orderBy('start', 'desc')
                ->take(1)
                ->get()->toArray();

        TravelExpense::create([
          'username' => auth()->user()->username ,
          'sub_date' => date('Y-m-d'),
          'sub_month' => $codt[0]['month'],
          'exp_type' => $request['expType'],
          'exp_status' => 0,
          'travel_type' => $request['selectTravelType'] ,
          'ticket_type' => $request['selectTicketType'] ,
          'travel_date' => $request['dateOfTravel'],
          'travel_from' => $request['selectFrom'],
          'travel_to' => $request['selectTo'],
          'exp_gross_amount' => $request['grossAmount'],
          'exp_distance' => null,
          'exp_refund' => $expRefund,
          'doc_url1' => $insert[0]['doc'],
          'doc_url2' => isset($insert[1]['doc']) ? $insert[1]['doc'] : null,
          'reviewed_by' => null,
          'comment' => null

        ]);
        if($request['expType'] == 'ht'){
          if($alert !== null){
            $errors = $alert ;
            return redirect('tes/myht')->withErrors($errors);
          }
          else{
            return redirect('tes/myht');
          }
        }
        elseif($request['expType'] == 'dc'){
          if($alert !== null){
            $errors = $alert ;
            return redirect('tes/mydc')->withErrors($errors);
          }
          else{
            return redirect('tes/mydc');
          }
        }
        else{
          dd('errr');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TravelExpense  $travelExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(TravelExpense $te)
    {
        TravelExpense::destroy($te->id);
        return redirect()->back();
    }


    // custom functions
    // users home travels only
    public function myht(){
      // get last two cutoff period

      $codt = \App\CutoffDates::where('start', '<', date('Y-m-d') )
              ->orderBy('start', 'desc')
              ->take(3)
              ->get()->toArray();
      //get current travel expense
      $travelExpense = \App\TravelExpense::where( 'username', '=', auth()->user()->username )
        -> where( 'exp_type' , 'ht' )
        -> where('sub_date' ,'>=',$codt[0]['start'])
        -> where('sub_date' ,'<=',$codt[0]['end'])
        ->get();
      // get last month travel expense
      $travelExpenseOld = \App\TravelExpense::where( 'username', '=', auth()->user()->username )
        -> where( 'exp_type' , 'ht' )
        -> where('sub_date' ,'>=',$codt[1]['start'])
        -> where('sub_date' ,'<=',$codt[1]['end'])
        ->get();
      $travelExpenseOldest = \App\TravelExpense::where( 'username', '=', auth()->user()->username )
        -> where( 'exp_type' , 'ht' )
        -> where('sub_date' ,'>=',$codt[2]['start'])
        -> where('sub_date' ,'<=',$codt[2]['end'])
        ->get();
      // get user details
      if(auth()->user()->perm_address != null){
        $pt = explode('__',decrypt(auth()->user()->perm_address));
        $userAddr['perm'] = $pt[2].', '.$pt[3];
      }
      if(auth()->user()->temp_address != null){
        $tt = explode('__',decrypt(auth()->user()->temp_address));
        $userAddr['temp'] = $tt[2].', '.$tt[3];
      }
      if(auth()->user()->temp_unoff_address != null){
        $tut = explode('__',decrypt(auth()->user()->temp_unoff_address));
        $userAddr['tempUO'] = $tut[2].', '.$tut[3];
      }
      return view('tes.myitems', compact('travelExpense','codt','travelExpenseOld','travelExpenseOldest','userAddr'));
    }

    //user's Commutes only
    public function mydc(){
      $codt = \App\CutoffDates::where('start', '<', date('Y-m-d') )
              ->orderBy('start', 'desc')
              ->take(3)
              ->get()->toArray();
      //get current travel expense
      $travelExpense = \App\TravelExpense::where( 'username', '=', auth()->user()->username )
        -> where( 'exp_type' , 'dc' )
        -> where('sub_date' ,'>=',$codt[0]['start'])
        -> where('sub_date' ,'<=',$codt[0]['end'])
        ->get();
      // get last month travel expense
      //get current travel expense
      $travelExpenseOld = \App\TravelExpense::where( 'username', '=', auth()->user()->username )
        -> where( 'exp_type' , 'dc' )
        -> where('sub_date' ,'>=',$codt[1]['start'])
        -> where('sub_date' ,'<=',$codt[1]['end'])
        ->get();
      $travelExpenseOldest = \App\TravelExpense::where( 'username', '=', auth()->user()->username )
        -> where( 'exp_type' , 'dc' )
        -> where('sub_date' ,'>=',$codt[2]['start'])
        -> where('sub_date' ,'<=',$codt[2]['end'])
        ->get();

        // get user details
        if(auth()->user()->perm_address != null){
          $pt = explode('__',decrypt(auth()->user()->perm_address));
          $userAddr['perm'] = $pt[2].', '.$pt[3];
        }
        if(auth()->user()->temp_address != null){
          $tt = explode('__',decrypt(auth()->user()->temp_address));
          $userAddr['temp'] = $tt[2].', '.$tt[3];
        }
        if(auth()->user()->temp_unoff_address != null){
          $tut = explode('__',decrypt(auth()->user()->temp_unoff_address));
          $userAddr['tempUO'] = $tut[2].', '.$tut[3];
        }
        $userAddr['office'] = 'Office, Kassák Lajos street';
      return view('tes.myitems', compact('travelExpense','codt','travelExpenseOld','travelExpenseOldest','userAddr'));
    }

    public function mycarCreate(){

        $year = date('Y');
        $month = date('F');

      $co = \App\CutoffDates::where('year', $year)->where('month',$month)->get();

      if(isset($co)){
        $arco = [];
        $start = strtotime( $co[0]['start'] );
        $end = strtotime( $co[0]['end'] );
        $end = strtotime('+1 days', $end);
        while($start !== $end){
          $arco[] = date('Y-m-d',$start);
          $start = strtotime('+1 days', $start);
        }


        return view('tes.createcarexp', compact('arco','month'));
      }
      else{
        dd("eee");
      }

    }

    public function mycarCreateCust(Request $request){
        $period = explode('-',$request['selectCarMonth']);
        $year = $period[0];
        $month = $period[1];
        $co = \App\CutoffDates::where('year', $year)->where('month',$month)->get()->toArray();

        if(isset($co)){
          $arco = [];
          $start = strtotime( $co[0]['start'] );
          $end = strtotime( $co[0]['end'] );
          $end = strtotime('+1 days', $end);
          while($start !== $end){
            $arco[] = date('Y-m-d',$start);
            $start = strtotime('+1 days', $start);
          }


          return view('tes.createcarexp', compact('arco','month'));
        }
        else{
          dd("eee");
        }

    }

    public function carexpstore(Request $request){
      request()->validate([
        'expType' => 'required|in:ht,dc',
        'selectTravelType' => 'in:public,car',
        'selectTicketType' => 'required_if:selectTravelType,public|in:ticket,planeticket,pass',
        'selectFrom' => 'required_if:selectTravelType,public|in:perm,temp,tempUO,office',
        'selectTo' => 'required_if:selectTravelType,public|in:perm,temp,tempUO,office',
        'grossAmount' => 'required_if:selectTravelType,public|numeric|min:1',
      ]);
      //select curroent cutoff


      $exps =[];
      $username = auth()->user()->username ;
      $commFrom = auth()->user()->daily_commute_from ;
      $dist = auth()->user()->commute_distance ;
      $carLimit = auth()->user()->car_allowance_limit ;
      $coCurr = \App\CutoffDates::where('year', date('Y'))->where('month',date('F'))->get()->toArray();

        $reqArr = $request->toArray();
        // get cutoff limits, and calculate current costs
        if($carLimit == 0 || $carLimit === null){
          foreach ($reqArr as $key => $value) {
            if($value == '1'){
              $travelDate = (explode('_', $key))[1];
              $checkexp = TravelExpense::where([
                ['username','=',$username] ,
                ['travel_date', '=', $travelDate],
                ['travel_from' ,'=', $commFrom],
                ['travel_to' ,'=', 'office'],
                ['exp_status' ,'<', 5]
              ])->exists();
              if($checkexp === false){
              $exps[] = [
                'username' => $username ,
                'sub_date' => date('Y-m-d'),
                'sub_month' => $coCurr[0]['month'],
                'exp_type' => 'dc',
                'exp_status' => 0,
                'travel_type' => 'car' ,
                'ticket_type' => null ,
                'travel_date' => $travelDate,
                'travel_from' => $commFrom,
                'travel_to' => 'office',
                'exp_gross_amount' => null,
                'exp_distance' => $dist,
                'exp_refund' => $dist * 15 ,
                'doc_url1' => null,
                'doc_url2' => null,
                'reviewed_by' => null,
                'comment' => null
              ];
            }
            $checkexp = TravelExpense::where([
              ['username','=',$username] ,
              ['travel_date', '=', $travelDate],
              ['travel_from' ,'=', 'office'],
              ['travel_to' ,'=', $commFrom],
              ['exp_status' ,'<', 5]
            ])->exists();
            if($checkexp === false){
              $exps[] = [
                'username' => $username ,
                'sub_date' => date('Y-m-d'),
                'sub_month' => $coCurr[0]['month'],
                'exp_type' => 'dc',
                'exp_status' => 0,
                'travel_type' => 'car' ,
                'ticket_type' => null ,
                'travel_date' => $travelDate,
                'travel_from' => 'office',
                'travel_to' => $commFrom,
                'exp_gross_amount' => null,
                'exp_distance' => $dist,
                'exp_refund' => $dist * 15 ,
                'doc_url1' => null,
                'doc_url2' => null,
                'reviewed_by' => null,
                'comment' => null
              ];
            }
          }
        }
      }
      else{ // in case ther is a limit for the user
        foreach ($reqArr as $key => $value) {
          if($value == '1'){
            $coday = (explode('_', $key))[1];
            $co = \App\CutoffDates::where('start','<=', $coday)->where('end','>=', $coday)->get()->toArray();
            break;
          }
        }
        if(!isset($co)){
          return redirect()->back()->withErrors(['error' => 'No date chosen.']);
        }

        $sumExp = DB::table('travel_expenses')->where([
          ['username','=',$username] ,
          ['travel_date', '>=', $co[0]['start'] ],
          ['travel_date', '<=', $co[0]['end'] ],
          ['exp_type', '=', 'dc'],
          ['travel_type', '=', 'car'],
          ['exp_status' ,'<', 5]
        ])->sum('exp_refund');

          foreach ($reqArr as $key => $value) {
            if($value == '1'){
              $travelDate = (explode('_', $key))[1];
              if($sumExp + ($dist*15) <= $carLimit ){
                $checkexp = TravelExpense::where([
                  ['username','=',$username] ,
                  ['travel_date', '=', $travelDate],
                  ['travel_from' ,'=', $commFrom],
                  ['travel_to' ,'=', 'office'],
                  ['exp_status' ,'<', 5]
                ])->exists();
                if($checkexp === false){
                $exps[] = [
                  'username' => $username ,
                  'sub_date' => date('Y-m-d'),
                  'sub_month' => $coCurr[0]['month'],
                  'exp_type' => 'dc',
                  'exp_status' => 0,
                  'travel_type' => 'car' ,
                  'ticket_type' => null ,
                  'travel_date' => $travelDate,
                  'travel_from' => $commFrom,
                  'travel_to' => 'office',
                  'exp_gross_amount' => null,
                  'exp_distance' => $dist,
                  'exp_refund' => $dist * 15 ,
                  'doc_url1' => null,
                  'doc_url2' => null,
                  'reviewed_by' => null,
                  'comment' => null
                ];
                $sumExp += $dist * 15;
              }
            }
            elseif($sumExp < $carLimit && $sumExp + ($dist*15) > $carLimit){

              $checkexp = TravelExpense::where([
                ['username','=',$username] ,
                ['travel_date', '=', $travelDate],
                ['travel_from' ,'=', $commFrom],
                ['travel_to' ,'=', 'office'],
                ['exp_status' ,'<', 5]
              ])->exists();
              if($checkexp === false){
                $partial = $carLimit - $sumExp;
                $exps[] = [
                  'username' => $username ,
                  'sub_date' => date('Y-m-d'),
                  'sub_month' => $coCurr[0]['month'],
                  'exp_type' => 'dc',
                  'exp_status' => 0,
                  'travel_type' => 'car' ,
                  'ticket_type' => null ,
                  'travel_date' => $travelDate,
                  'travel_from' => $commFrom,
                  'travel_to' => 'office',
                  'exp_gross_amount' => null,
                  'exp_distance' => $dist,
                  'exp_refund' => $partial ,
                  'doc_url1' => null,
                  'doc_url2' => null,
                  'reviewed_by' => null,
                  'comment' => null
                ];
                break;
              }
            }
            else{
              break;
            }
            if($sumExp + ($dist*15) < $carLimit){
              $checkexp = TravelExpense::where([
                ['username','=',$username] ,
                ['travel_date', '=', $travelDate],
                ['travel_from' ,'=', 'office'],
                ['travel_to' ,'=', $commFrom],
                ['exp_status' ,'<', 5]
              ])->exists();
              if($checkexp === false){
                $exps[] = [
                  'username' => $username ,
                  'sub_date' => date('Y-m-d'),
                  'sub_month' => $coCurr[0]['month'],
                  'exp_type' => 'dc',
                  'exp_status' => 0,
                  'travel_type' => 'car' ,
                  'ticket_type' => null ,
                  'travel_date' => $travelDate,
                  'travel_from' => 'office',
                  'travel_to' => $commFrom,
                  'exp_gross_amount' => null,
                  'exp_distance' => $dist,
                  'exp_refund' => $dist * 15 ,
                  'doc_url1' => null,
                  'doc_url2' => null,
                  'reviewed_by' => null,
                  'comment' => null
                ];

                $sumExp += $dist * 15;
              }
            }
            elseif($sumExp < $carLimit && $sumExp + ($dist*15) > $carLimit){

              $checkexp = TravelExpense::where([
                ['username','=',$username] ,
                ['travel_date', '=', $travelDate],
                ['travel_from' ,'=', 'office'],
                ['travel_to' ,'=', $commFrom],
                ['exp_status' ,'<', 5]
              ])->exists();
              if($checkexp === false){
                $partial = $carLimit - $sumExp;
                $exps[] = [
                  'username' => $username ,
                  'sub_date' => date('Y-m-d'),
                  'sub_month' => $coCurr[0]['month'],
                  'exp_type' => 'dc',
                  'exp_status' => 0,
                  'travel_type' => 'car' ,
                  'ticket_type' => null ,
                  'travel_date' => $travelDate,
                  'travel_from' => 'office',
                  'travel_to' => $commFrom,
                  'exp_gross_amount' => null,
                  'exp_distance' => $dist,
                  'exp_refund' => $partial ,
                  'doc_url1' => null,
                  'doc_url2' => null,
                  'reviewed_by' => null,
                  'comment' => null
                ];
                break;
              }
            }
            else{
              break;
            }
          }
        }
      }

      if(isset($exps[0])){
        DB::table('travel_expenses')->insert($exps);
        return redirect('tes/mydc');
      }
      else{
          return redirect('tes/mydc')->withErrors(['error' => 'Could not add any items. Maybe you have try to add duplicated items.']);
      }

    }
}
