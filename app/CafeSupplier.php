<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CafeSupplier extends Model
{
    const UPDATED_AT = 'modified_at';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function elements()
    {
        return $this->hasMany('App\CafeElement');
    }

}
