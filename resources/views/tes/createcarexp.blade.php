@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1 class="mc-auto">Add New Daily Commute Expense Item</h1>
  </div>
  @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">

      <div class="forCar">
      <table class="table .table-striped .table-hover" style="width:100%">
        <tr>
          <td> {{ __('Payroll Period')}} </td>
          <td>

            <form class="" action="{{ route('tes.mydc.create.mycarcreatecustom') }}" method="post">
              @csrf
              <select class="" name="selectCarMonth" onchange="this.form.submit()">
                <option value="{{ date('Y-F', time()) }}" {{ $month == date('F', time() ) ? 'selected':'' }} >{{ date('Y M', time()) }}</option>
                <option value="{{ date('Y-F', strtotime('-1 months' )) }}" {{ $month == date('F', strtotime('-1 months' )) ? 'selected':'' }}>{{ date('Y M', strtotime('-1 months' )) }}</option>
                <option value="{{ date('Y-F', strtotime('-2 months' )) }}" {{ $month == date('F', strtotime('-2 months' )) ? 'selected':'' }}>{{ date('Y M', strtotime('-2 months' )) }}</option>
                <option value="{{ date('Y-F', strtotime('-3 months' )) }}" {{ $month == date('F', strtotime('-3 months' )) ? 'selected':'' }}>{{ date('Y M', strtotime('-3 months' )) }}</option>
                <option value="{{ date('Y-F', strtotime('-4 months' )) }}" {{ $month == date('F', strtotime('-4 months' )) ? 'selected':'' }}>{{ date('Y M', strtotime('-4 months' )) }}</option>
                <option value="{{ date('Y-F', strtotime('-5 months' )) }}" {{ $month == date('F', strtotime('-5 months' )) ? 'selected':'' }}>{{ date('Y M', strtotime('-5 months' )) }}</option>
              </select>
            </form>
          </td>
        </tr>
        <tr>
          <td>{{ __('Travel Type')}}</td>
          <td>
            <select id="selectTT_id" class="" name="selectTravelType" onchange="traveltypereroute()">
              <option value="public">{{ __('Public Transport')}}</option>
              <option value="car" selected>{{ __('Car')}}</option>
            </select>
          </td>
        </tr>
        <tr>
          <td colspan="2">{{ __('Calculated distance from office:')}}</td>
        </tr>
        <tr>
          <td colspan="2">{{ auth()->user()->commute_distance }} km</td>
        </tr>
        <form class="" action="{{ route('tes.mydc.carexpstore') }}" method="post" >
          @csrf
          <input type="hidden" name="expType" value="dc"/>
          <input type="hidden" name="selectTravelType" value="car"/>
          @if(isset($arco))
          <tr>
            <td colspan="2">
              <table cellspacing="3" class="carcalendar">
                <tr>
                  <th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th>
                </tr>
                @if(date('D',strtotime($arco[0])) == 'Tue')
                  <tr><td></td>
                @endif
                @if(date('D',strtotime($arco[0])) == 'Wed')
                  <tr><td colspan="2"></td>
                @endif
                @if(date('D',strtotime($arco[0])) == 'Thu')
                  <tr><td colspan="3"></td>
                @endif
                @if(date('D',strtotime($arco[0])) == 'Fri')
                  <tr><td colspan="4"></td>
                @endif
                @if(date('D',strtotime($arco[0])) == 'Sat')
                  <tr><td colspan="5"></td>
                @endif
                @if(date('D',strtotime($arco[0])) == 'Sun')
                  <tr><td colspan="6"></td>
                @endif

                @foreach ($arco as $key => $day)
                  @if(date('D',strtotime($day)) == 'Mon')
                  <tr>
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>
                  @endif
                  @if(date('D',strtotime($day)) == 'Tue')
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>
                  @endif
                  @if(date('D',strtotime($day)) == 'Wed')
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>
                  @endif
                  @if(date('D',strtotime($day)) == 'Thu')
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>
                  @endif
                  @if(date('D',strtotime($day)) == 'Fri')
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>
                  @endif
                  @if(date('D',strtotime($day)) == 'Sat')
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>
                  @endif
                  @if(date('D',strtotime($day)) == 'Sun')
                    <td class="carDayCheckboxTD" >
                      <input class="carDayCheckbox hidden" type="checkbox" name="day_{{$day}}" value="1"></input>
                       {{ $day }}
                     </td>

                  @endif
                @endforeach
                @if(date('D',strtotime(end($arco))) == 'Mon')
                  <td colspan="6"></td></tr>
                @endif
                @if(date('D',strtotime(end($arco))) == 'Tue')
                  <td colspan="5"></td></tr>
                @endif
                @if(date('D',strtotime(end($arco))) == 'Wed')
                  <td colspan="4"></td></tr>
                @endif
                @if(date('D',strtotime(end($arco))) == 'Thu')
                  <td colspan="3"></td></tr>
                @endif
                @if(date('D',strtotime(end($arco))) == 'Fri')
                  <td colspan="2"></td></tr>
                @endif
                @if(date('D',strtotime(end($arco))) == 'Sat')
                  <td ></td></tr>
                @endif

              </table>
            </td>
          </tr>


          @endif



          </table>
          <button class="btn btn-primary" type="submit" name="button">Save</button>
          <button class="btn btn-primary" type="button" name="cancel">Cancel</button>

        </form>
      </div>

  </div>
</div>
@endsection
