<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\TravelExpense;
use Illuminate\Support\Facades\DB;

class ValidateExpense
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //validate date, monthly limits, dierctions, car allowance and limits
        $errors = new \Illuminate\Support\MessageBag();
        $alerts = new \Illuminate\Support\MessageBag();
        $user = auth() -> user();
        //check if car is allowed
        if($request['selectTravelType'] === 'car' && $user -> car_allowed !== 1){
          $errors->add('carNotAllowed', 'You cannot submit car expenses. If you are eligable for it please contact the payroll team to authorize it.');
          return redirect() -> back() -> withErrors($errors)  ;
        }

        if($request -> file() == null && $request['selectTravelType'] === 'public'){
          $errors->add('nofile', 'No files attached!');
        }
        if($request['selectTravelType'] === 'public'){
          // check if start and end are not the same address
          if($request['selectFrom'] === $request['selectTo']){
            $errors->add('sameDest', 'Your departure and destination location cannot be the same!');
          }
          // check if travel date is within Joining & leaving date
          if(decrypt($user -> joining_date) >= $request['dateOfTravel']){
            $errors->add('beforeJoin', 'Your travel must be after your joining date!');
          }

          if($user -> termination_date != null && decrypt($user -> termination_date) >= $request['dateOfTravel']){
            $errors->add('afterTerm', 'Your travel must be before your termination date!');
          }

          // check if travel date exists for users in the same direction
          $checkexp = TravelExpense::where([
            ['username','=',$user -> username] ,
            ['travel_date', '=', $request['dateOfTravel']],
            ['travel_from' ,'=', $request['selectFrom']],
            ['travel_to' ,'=', $request['selectTo']],
            ['exp_status' ,'<', 5]
          ])->exists();
          if($checkexp){
            $errors->add('expExists', 'An expense item already exists with the same date and route');
          }

          if(count($request -> file('docs')) === 1 ){
            if($request['selectTicketType'] === 'planeticket' && $request['selectTravelType'] === 'public'){
              $errors->add('twoFilesReq', 'Please attach the invoice and the boarding pass!');
            }
            if($request['selectTicketType'] === 'pass' && $request['selectTravelType'] === 'public'){
              $errors->add('twoFilesReq', 'Please attach the invoice and pass!');
            }
          }

          if(count($request -> file('docs')) === 2){

            if($request['selectTicketType'] === 'ticket' && $request['selectTravelType'] === 'public'){
              $errors->add('tooManyFiles', 'Too many files attached! You only need to attach a copy of the ticket!');
            }
          }

          if(count($request -> file('docs')) > 2){
            $errors->add('tooManyFiles', 'Too many files attached!');
          }

          //check filetype
          $allowedExt = ['jpg','jpeg','png','bmp','pdf'];
          $docs = $request -> file('docs');
          foreach ($docs as $doc) {
            $docExt = $doc->getClientOriginalExtension();
            if(!in_array($docExt, $allowedExt)){
              $errors->add('invalidExt', 'The uploaded file has invalid extension type. Allowed types: jpg, jpeg, png, bmp, pdf .');
            }
          }

        }

        if($request['selectTravelType'] !== 'public' && $request['expType'] == 'ht'){
          $errors->add('noHtCar', ' Home travel by car cannot be reimbursed!');
        }

        if($request['selectTicketType'] === 'planeticket' && $request['expType'] == 'dc'){
          $errors->add('noPlaneDc', ' Daily Cummute by plane cannot be reimbursed!');
        }

        if($request['selectTicketType'] === 'pass' && $request['expType'] == 'ht'){
          $errors->add('noPassHt', ' Home Travel by monthly pass cannot be reimbursed!');
        }
        // check monthly limits 37200 huf
        if($request['selectTravelType'] === 'public' && $request['expType'] == 'ht'){
          $expLimit = DB::table('travel_expenses')->where([
            ['username','=',$user -> username] ,
            ['travel_date', '>=', (substr($request['dateOfTravel'],0,8)).'01' ],
            ['travel_date', '<=', (substr($request['dateOfTravel'],0,8)).'31' ],
            ['exp_type', '=', 'ht'],
            ['exp_status' ,'<', 5]
          ])->sum('exp_refund');

          if($expLimit >= 37200){
            $errors->add('publOverLimit', 'Cannot add expense as the summary of expenses for the month are already reached the limit!');
          }
          if($expLimit + ($request['grossAmount']*0.86) > 37200){
            $partial = 37200 - $expLimit ;
            $alerts->add('publPartiallyOverLimit', 'You have reached the monthly reimbursement limit, but the last item has been added partially.');
          }
          // add partial refound here until 37200
        }
        //The Daily Cummute within Budapest cannot be reimbursed.
        $commFrom = $user -> daily_commute_from ;
        if($user -> perm_address != null){
          $permAddr = decrypt($user -> perm_address) ;
          $allAddr['perm'] = $permAddr ;
        }
        if($user -> temp_address != null){
          $tempAddr = decrypt($user -> temp_address) ;
          $allAddr['temp'] = $tempAddr ;
        }
        if($user -> temp_unoff_address != null){
          $tempUOAddr = decrypt($user -> temp_unoff_address) ;
          $allAddr['tempUO'] = $tempUOAddr ;
        }
        if($request['selectTravelType'] === 'public'){
          if($request['selectFrom'] != 'office' && !isset($allAddr[$request['selectFrom']])){
            $errors->add('missingAddr', 'The chosen Address is not set up, please add it under your profile!');
          }
          if($request['selectTo'] != 'office' && !isset($allAddr[$request['selectTo']])){
            $errors->add('missingAddr', 'The chosen Address is not set up, please add it under your profile!');
          }
        }

        switch ($commFrom) {
          case 'perm':
            if($user -> perm_address != null){
              $commAddr =  $permAddr;
            }
            else{
              $errors->add('missingAddr', 'The Permanent Address is not set up, please add it under your profile!');
            }
            break;
          case 'temp':
            if($user -> temp_address != null){
              $commAddr = $tempAddr ;
            }
            else{
              $errors->add('missingAddr', 'The Temporary Address is not set up, please add it under your profile!');
            }
            break;
          case 'tempUO':
            if($user -> temp_unoff_address != null){
              $commAddr = $tempUOAddr ;
            }
            else{
              $errors->add('missingAddr', 'The Unofficial Temporary Address is not set up, please add it under your profile!');
            }
            break;
          default:
          if($user -> perm_address != null){
            $commAddr =  $permAddr;
          }
          else{
            $errors->add('missingAddr', 'The Permanent Address is not set up, please add it under your profile!');
          }
            break;
        }

        if(stripos($commAddr, 'budapest') != false && $request['expType'] == 'dc'){
          $errors->add('dcBuda', 'The Daily Cummute within Budapest cannot be reimbursed.');
        }
        // car specific checks
        if($request['selectTravelType'] === 'car'){
          // check if commute addr is added -> this is already done
            $expLimit = DB::table('travel_expenses')->where([
              ['username','=',$user -> username] ,
              ['travel_date', '>=', (substr($request['dateOfTravel'],0,8)).'01' ],
              ['travel_date', '<=', (substr($request['dateOfTravel'],0,8)).'31' ],
              ['exp_type', '=', 'dc'],
              ['travel_type', '=', 'car'],
              ['exp_status' ,'<', 5]
            ])->sum('exp_refund');

          //check if exp exists
          $reqArr = $request->toArray();
          foreach ($reqArr as $key => $value) {
            if($value == '1'){
              $travelDate = (explode('_', $key))[1];

              $checkexpToO = TravelExpense::where([
                ['username','=',$user -> username] ,
                ['travel_date', '=', $travelDate],
                ['travel_from' ,'=', $commFrom],
                ['travel_to' ,'=', 'office'],
                ['exp_type' ,'=', 'dc'],
                ['exp_status' ,'<', 5]
              ])->exists();
              $checkexpFromO = TravelExpense::where([
                ['username','=',$user -> username] ,
                ['travel_date', '=', $travelDate],
                ['travel_from' ,'=', 'office'],
                ['travel_to' ,'=', $commFrom],
                ['exp_type' ,'=', 'dc'],
                ['exp_status' ,'<', 5]
              ])->exists();

              if($checkexpToO || $checkexpFromO){
                $alerts->add('expExists', 'A car expense item already exists with the same date('.$travelDate.') and route');
              }

              if($user->car_allowance_limit != null && $user->car_allowance_limit <= $expLimit){
                $alerts->add('publOverLimit', 'Some expense could not been added, as you have reached your monthly limit.');

              }
              if($user->car_allowance_limit != null && $expLimit + ($user->commute_distance * 15 * 2) > $user->car_allowance_limit){
                $alerts->add('publPartiallyOverLimit', 'You have reached the monthly reimbursement limit, but the last item has been added partially.');
              }


            }
          }
          //check limit
          // check joining, leaving date
        }


        if( $request['selectTicketType'] == 'planeticket' &&
            (stripos($allAddr[$request['selectFrom']], 'hungary') !== false ||
            stripos($allAddr[$request['selectFrom']], 'magyarország') !== false) &&
            (stripos($allAddr[$request['selectTo']], 'hungary') !== false ||
            stripos($allAddr[$request['selectTo']], 'magyarország') !== false) ){
              $errors->add('domeyticFlight', 'You cannot submit domestic flight expense.');
            }


        // return errors or continue
        if ($errors->any()) {
          //dd($errors);
          return redirect() -> back() -> withErrors($errors)->withInput()  ;
        }
        else{
          if(isset($partial)){
            $request -> session()->flash('partial' , $partial );
            $request -> session()->flash('alerts' , $alerts );
          }

          return $next($request);
        }

    }


    public function terminate($request, $response)
    {
        // Store the session data...
    }
}
