@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">

@if(Route::current()->getName() == 'tes.myht')
  <h1 class="mc-auto">{{__('My Home Travel Expenses')}}</h1>
@elseif(Route::current()->getName() == 'tes.mydc')
  <h1 class="mc-auto">{{__('My Daily Commute Expenses')}}</h1>
@else
  <h1 class="mc-auto">{{__('My Expenses')}}</h1>
@endif
</div>
  @include('layouts.errordiv')
<div class="container mc-auto bg-white shadow-sm">
  <h3 class="mc-auto">Current Payroll period (Open until {{ $codt[0]['end']}})</h3>
  <table class="expTable table .table-striped .table-hover" style="width:100%">
    <thead class="thead-dark">
      <tr>
        <th>{{__('Travel Type')}}</th>
        <th>{{__('Ticket Type')}}</th>
        <th>{{__('Submission Month')}}</th>
        <th>{{__('Travel Date')}}</th>
        <th>{{__('From')}}</th>
        <th>{{__('To')}}</th>
        <th>{{__('Gross Amount')}}</th>
        <th>{{__('Distance')}}</th>
        <th>{{__('Refund')}}</th>
        <th>{{__('Status')}}</th>
        <th>{{__('Actions')}}</th>
      </tr>
    </thead>
  <tbody>

      @foreach ($travelExpense as $exp)
      <tr>
          <td> {{ $exp->travel_type == 'public' ? __('Public Transport') : __('Car')}} </td>
          <td>
            @if($exp->ticket_type == 'ticket')
              {{__('Ticket')}}
            @elseif($exp->ticket_type == 'planeticket')
              {{__('Plane Ticket')}}
            @elseif($exp->ticket_type == 'pass')
                {{__('Monthly Pass')}}
            @else
              {{__('N/A')}}
            @endif
          </td>
          <td> {{ __($exp->sub_month) }} </td>
          <td> {{ $exp->travel_date }} </td>
          <td> {{ $userAddr[$exp->travel_from] }} </td>
          <td> {{ $userAddr[$exp->travel_to] }} </td>
          <td> {{ $exp->exp_gross_amount }}  </td>
          <td> {{ $exp->exp_distance }} </td>
          <td class="refund_cell"> {{ $exp->exp_refund }}  </td>
          @if($exp->exp_status == 0)
          <td> {{__('Pending')}} </td>
          @elseif($exp->exp_status == 2)
          <td> {{__('Approved')}} </td>
          @else
          <td> {{__('Rejected')}} </td>
          @endif
          <td>
            <form class="" action="{{ route('tes.destroy',$exp->id) }}" method="post">
              {{ method_field('DELETE') }}
              @csrf
              <button class="btn btn-primary" type="submit" name="delExp"><span class="glyphicon glyphicon-trash"></span></button>
            </form>
          </td>
      </tr>
      @endforeach

  </tbody>
  <tfoot class="tfoot-dark">
    <tr>
      <td colspan="4" >{{__('Summary:')}}</td>
      <td colspan="4"><span class="num_items_cell"></span>  {{__(' items')}}</td>
      <td colspan="3"><span class="sum_items_cell"></span> HUF</td>
    </tr>
  </tfoot>

  </table>
  @if(Route::current()->getName() == 'tes.myht')
    <a href="{{ route( 'tes.myht.create' ) }}"> <button class="btn btn-primary" type="button" name="button">Add New Item</button> </a>
  @elseif(Route::current()->getName() == 'tes.mydc')
    <a href="{{ route( 'tes.mydc.create' ) }}"> <button class="btn btn-primary" type="button" name="button">Add New Item</button> </a>
  @else
    <a href="{{ route( 'tes.myht.create' ) }}"> <button class="btn btn-primary" type="button" name="button">Add New Item</button> </a>
  @endif
  <hr>
  <h3 class="mc-auto">Last Payroll periods(Closed)</h3>
  <table class="expTable table .table-striped .table-hover" style="width:100%">
    <thead class="thead-dark">
      <tr  class="tableshrink">
        <th colspan="11" >{{ $codt[1]['year']}} {{ $codt[1]['month']}}</th>
      </tr>
      <tr class="headerlist hidden">
        <th>{{__('Travel Type')}}</th>
        <th>{{__('Ticket Type')}}</th>
        <th>{{__('Submission Month')}}</th>
        <th>{{__('Travel Date')}}</th>
        <th>{{__('From')}}</th>
        <th>{{__('To')}}</th>
        <th>{{__('Gross Amount')}}</th>
        <th>{{__('Distance')}}</th>
        <th>{{__('Refund')}}</th>
        <th>{{__('Status')}}</th>
        <th>{{__('Comment')}}</th>
      </tr>
    </thead>
  <tbody class="hidden">
      @if($travelExpenseOld -> isEmpty())
        <td colspan="11"> {{ __('Nothing to display...')}} </td>
      @else
        @foreach ($travelExpenseOld as $exp1)
        <tr>
          <td> {{ $exp1->travel_type == 'public' ? __('Public Transport') : __('Car')}} </td>
          <td>
            @if($exp1->ticket_type == 'ticket')
              {{__('Ticket')}}
            @elseif($exp1->ticket_type == 'planeticket')
              {{__('Plane Ticket')}}
            @elseif($exp1->ticket_type == 'pass')
                {{__('Monthly Pass')}}
            @else
              {{__('N/A')}}
            @endif
          </td>
          <td> {{ __($exp1->sub_month) }} </td>
          <td> {{ $exp1->travel_date }} </td>
          <td> {{ $userAddr[$exp1->travel_from] }} </td>
          <td> {{ $userAddr[$exp1->travel_to] }} </td>
          <td> {{ $exp1->exp_gross_amount }}  </td>
          <td> {{ $exp1->exp_distance }} </td>
          <td class="refund_cell"> {{ $exp1->exp_refund }}  </td>
          @if($exp1->exp_status == 0)
          <td> {{__('Pending')}} </td>
          @elseif($exp1->exp_status == 2)
          <td> {{__('Approved')}} </td>
          @else
          <td> {{__('Rejected')}} </td>
          @endif
          <td> {{ $exp1->comment }} </td>
        </tr>
        @endforeach
      @endif

  </tbody>
  <tfoot class="tfoot-dark">
    <tr>
      <td colspan="4" >{{__('Summary:')}}</td>
      <td colspan="4"><span class="num_items_cell"></span>  {{__(' items')}}</td>
      <td colspan="3"><span class="sum_items_cell"></span> HUF</td>
    </tr>
  </tfoot>
  </table>

  <table class="expTable table .table-striped .table-hover" style="width:100%">
    <thead class="thead-dark">
      <tr  class="tableshrink">
        <th colspan="11" >{{ $codt[2]['year']}} {{ $codt[2]['month']}}</th>
      </tr>
      <tr class="headerlist hidden">
        <th>{{__('Travel Type')}}</th>
        <th>{{__('Ticket Type')}}</th>
        <th>{{__('Submission Month')}}</th>
        <th>{{__('Travel Date')}}</th>
        <th>{{__('From')}}</th>
        <th>{{__('To')}}</th>
        <th>{{__('Gross Amount')}}</th>
        <th>{{__('Distance')}}</th>
        <th>{{__('Refund')}}</th>
        <th>{{__('Status')}}</th>
        <th>{{__('Comment')}}</th>
      </tr>
    </thead>
  <tbody class="hidden">

      @if($travelExpenseOldest -> isEmpty())
        <td colspan="11"> {{ __('Nothing to display...')}} </td>
      @else
        @foreach ($travelExpenseOldest as $exp2)
        <tr>
          <td> {{ $exp2->travel_type == 'public' ? __('Public Transport') : __('Car')}} </td>
          <td>
            @if($exp2->ticket_type == 'ticket')
              {{__('Ticket')}}
            @elseif($exp2->ticket_type == 'planeticket')
              {{__('Plane Ticket')}}
            @elseif($exp2->ticket_type == 'pass')
                {{__('Monthly Pass')}}
            @else
              {{__('N/A')}}
            @endif
          </td>
          <td> {{ __($exp2->sub_month) }} </td>
          <td> {{ $exp2->travel_date }} </td>
          <td> {{ $userAddr[$exp2->travel_from] }} </td>
          <td> {{ $userAddr[$exp2->travel_to] }} </td>
          <td> {{ $exp2->exp_gross_amount }}  </td>
          <td> {{ $exp2->exp_distance }} </td>
          <td class="refund_cell"> {{ $exp2->exp_refund }}  </td>
          @if($exp2->exp_status == 0)
          <td> {{__('Pending')}} </td>
          @elseif($exp2->exp_status == 2)
          <td> {{__('Approved')}} </td>
          @else
          <td> {{__('Rejected')}} </td>
          @endif
          <td> {{ $exp2->comment }} </td>
        </tr>
        @endforeach
      @endif
  </tbody>
  <tfoot class="tfoot-dark">
    <tr>
      <td colspan="4" >{{__('Summary:')}}</td>
      <td colspan="4"><span class="num_items_cell"></span>  {{__(' items')}}</td>
      <td colspan="3"><span class="sum_items_cell"></span> HUF</td>
    </tr>
  </tfoot>
  </table>

</div>
@endsection
