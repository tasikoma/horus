<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCafeElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cafe_elements', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('name');
          $table->unsignedBigInteger('supplier_id');
          $table->string('description',255);
          $table->string('monthly_limit')->nullable();
          $table->string('tax')->nullable();
          $table->string('breachtax')->nullable();
          $table->string('yearly_limit')->nullable();
          $table->boolean('status');
          $table->tinyInteger('is_mandatory')->nullable();
          $table->tinyInteger('needs_fallback')->nullable();
          $table->string('added_by')->nullable();
          $table->timestamps();
          $table->foreign('supplier_id')->references('id')->on('cafe_suppliers')
          ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cafe_elements');
    }
}
