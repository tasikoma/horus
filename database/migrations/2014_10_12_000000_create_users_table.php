<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('users')) {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('user_status');
            $table->string('joining_date');
            $table->string('termination_date')->nullable();
            $table->timestamp('last_logon')->nullable();
            $table->string('fullname');
            $table->string('tax_id')->nullable();
            $table->string('perm_address')->nullable();
            $table->string('temp_address')->nullable();
            $table->string('temp_unoff_address')->nullable();
            $table->string('daily_commute_from')->nullable();
            $table->boolean('car_allowed')->nullable();
            $table->bigInteger('car_allowance_limit')->nullable();
            $table->string('licence_plate')->nullable();
            $table->string('preferred_lang')->nullable();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
