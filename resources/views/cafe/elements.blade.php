@extends('layouts.homeLayout')
@section('content')
<div class="container-fluid mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1>Cafeteria Element Management</h1>
  </div>
  @include('layouts.errordiv')
  <div class="container.fluid mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      <thead class="thead-dark">
        <tr>
          <th>{{ __('Element Name') }}</th>
          <th>{{ __('Element Family') }}</th>
          <th>{{ __('Supplier') }}</th>
          <th>{{ __('Description') }}</th>
          <th>{{ __('Monthly limit') }}</th>
          <th>{{ __('Fix amount') }}</th>
          <th>{{ __('Element can be over the limit?') }}</th>
          <th>{{ __('Muliplier') }}</th>
          <th>{{ __('Total Tax') }}</th>
          <th>{{ __('Total Tax - over limit') }}</th>
          <th>{{ __('Yearly limit') }}</th>
          <th>{{ __('Mandatory') }}</th>
          <th>{{ __('Fallback Required') }}</th>
          <th>{{ __('Source to fund') }}</th>
          <th>{{ __('Months allowed') }}</th>
          <th>{{ __('Actions') }}</th>
        </tr>
      </thead>
      <tbody>
        @foreach($elements as $element)
        <tr>
          <td>{{ $element->name }}</td>
          <td>{{ $element->family_id != null ? $element->family->name : '' }}</td>
          <td>{{ $element->supplier->name }}</td>
          <td>{{ $element->description }}</td>
          <td>{{ $element->monthly_limit }}</td>
          <td>{{ $element->fix_amount }}</td>
          <td>{{ $element->limit_breachable }}</td>
          <td>{{ $element->multiplier }}</td>
          <td>{{ $element->tax }} %</td>
          <td>{{ $element->breachtax }} %</td>
          <td>{{ $element->yearly_limit }}</td>
          <td>{{ $element->is_mandatory }}</td>
          <td>{{ $element->needs_fallback }}</td>
          <td>{{ $element->fund_source }}</td>
          <td>{{ $element->allowed_months }}</td>
          <td>
            <a href="{{ route('elements.edit', $element->id) }}">
              <button class="btn btn-primary" type="button" name="editElementButton">
                <span class="glyphicon glyphicon-edit"></span>
              </button>
            </a>
            <form class="" action="{{ route('elements.destroy', $element->id) }}" method="post" style="display:inline;">
              @csrf
              {{ method_field('DELETE') }}
              <button class="btn btn-primary" type="submit" name="disableElementButton">
                <span class="glyphicon glyphicon-trash"></span>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
      <tfoot class="tfoot-dark">
        <tr>
          <td colspan="16" style="text-align:center;">
            <a href="{{ route('elements.create') }}">
              <button class="btn btn-primary"  type="button" name="addElementBtn">{{ __('Add New Element') }}</button>
            </a>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
@endsection
