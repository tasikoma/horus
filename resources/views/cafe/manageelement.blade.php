@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    @if($act == 'new')
      <h1>{{ __('Add New Cafeteria Element') }}</h1>
    @else
      <h1>{{ __('Edit Cafeteria Element') }}</h1>
    @endif
  </div>
  @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      @if($act == 'new')
      <form class="" action="{{ route('elements.store')}}" method="post">
      @else
      <form class="" action="{{ route('elements.update', $element->id)}}" method="post">
        {{ method_field('PATCH') }}
      @endif
        @csrf
        <tr>
          <td>{{ __('Name:') }}</td>
          <td>
            <input type="text" name="elementName" value="{{ isset($element) ? $element->name : '' }}" placeholder="{{ __('Name of the element') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Element Family:') }}</td>
          <td>
            <select class="" name="elementSelectFamily">
              <option value="0" {{ isset($element) &&  $element->family_id == null ? 'selected': ''}} > {{__('None')}}</option>
              @foreach($fams as $fam)
                <option value="{{ $fam->id }}" {{ isset($element) &&  $element->family_id == $fam->id ? 'selected':''}}> {{ $fam->name }} </option>
              @endforeach
            </select>
          </td>
        </tr>
        <tr>
          <td>{{ __('Supplier:') }}</td>
          <td>
            <select class="" name="elementSelectSupplier">
              @foreach($sups as $sup)
                <option value="{{ $sup->id }}" {{ isset($element) &&  $element->supplier_id == $sup->id ? 'selected':''}}> {{ $sup->name }} </option>
              @endforeach
            </select>
          </td>
        </tr>
        <tr>
          <td>{{ __('Description:') }}</td>
          <td>
            <textarea name="elementDescription" rows="4"  placeholder="{{ __('Description of element: limits, taxes, etc') }}">{{ isset($element) ? $element->description : '' }}</textarea>
          </td>
        </tr>
        <tr>
          <td>{{ __('Monthly limit(Net):') }}</td>
          <td>
            <input type="text" name="elementMLimit" value="{{ isset($element) ? $element->monthly_limit : '' }}" placeholder="{{ __('Monthly limit in HUF') }}"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Fix amount:(fill only if eleemnt has a fix monthly amount)') }}</td>
          <td>
            <input type="text" name="elementFix" value="{{ isset($element) ? $element->fix_amount : '' }}" placeholder="{{ __('Monthly fix amount in HUF') }}"/>
          </td>
        </tr>
        <tr>
          <td>{{ __('Limit can be breached:') }}</td>
          <td>
            <input type="checkbox" name="elementLimitBrachable" value="1" {{ isset($element) && $element->limit_breachable == 1 ? 'checked' : '' }}>
          </td>
        </tr>
        <tr>
          <td>{{ __('Element Multiplier:') }}</td>
          <td>
            <input type="text" name="elementMultiplier" value="{{ isset($element) ? $element->multiplier : '' }}" placeholder="{{ __('1.0 if taxfree...') }}" />
          </td>
        </tr>
        <tr>
          <td>{{ __('Tax:') }}</td>
          <td>
            <input type="text" name="elementTax" value="{{ isset($element) ? $element->tax : '' }}" placeholder="{{ __('Tax percentage') }}" />
          </td>
        </tr>
        <tr>
          <td>{{ __('Tax - Over limit:') }}</td>
          <td>
            <input type="text" name="elementBreachTax" value="{{ isset($element) ? $element->breachtax : '' }}" placeholder="{{ __('Tax - over limit') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Yearly Limit(Net):') }}</td>
          <td>
            <input type="text" name="elementYLimit" value="{{ isset($element) ? $element->yearly_limit : '' }}" placeholder="{{ __('Yearly limit in HUF') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Manadatory:') }}</td>
          <td>
            <input type="checkbox" name="elementMandatory" value="1" {{ isset($element) && $element->is_mandatory == 1 ? 'checked' : '' }}>
          </td>
        </tr>
        <tr>
          <td>{{ __('Element needs fallback:') }}</td>
          <td>
            <input type="checkbox" name="elementNeedsFallback" value="1" {{ isset($element) && $element->needs_fallback == 1 ? 'checked' : '' }}>
          </td>
        </tr>
        <tr>
          <td>{{ __('Element Funded from:') }}</td>
          <td>
            <select class="" name="selectElementFund">
              <option value="cafe" {{ isset($element) && $element->fund_source == 'cafe' ? 'selected' : ''  }}>{{ __('Cafeteria Allowance') }}</option>
              <option value="salary" {{ isset($element) && $element->fund_source == 'salary' ? 'selected' : ''  }}>{{ __('Salary') }}</option>
            </select>
          </td>
        </tr>
        <tr>
          <td colspan="2">{{ __('Months the element can be requested:') }}</td>
        </tr>
        <tr>
          <td colspan="2">
            <table>
              <tr>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('all',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }} {{ !(isset($element)) ? 'highlightedMonth':'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthAll" value="all" {{ isset($element) && (stripos('all',$element->allowed_months) !== false) ? 'checked' :'' }}
                  {{ !(isset($element)) ? 'checked':'' }}/>
                  {{ __('All') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('jan',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthJan" value="jan" {{ isset($element) && (stripos('jan',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Jan') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('feb',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthFeb" value="feb" {{ isset($element) && (stripos('feb',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Feb') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('mar',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthMar" value="mar" {{ isset($element) && (stripos('mar',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Mar') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('apr',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthApr" value="apr" {{ isset($element) && (stripos('apr',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Apr') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('may',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthMay" value="may" {{ isset($element) && (stripos('may',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('May') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('jun',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthJun" value="jun" {{ isset($element) && (stripos('jun',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Jun') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('jul',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthJul" value="jul" {{ isset($element) && (stripos('jul',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Jul') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('aug',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthAug" value="aug" {{ isset($element) && (stripos('aug',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Aug') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('sep',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthSep" value="sep" {{ isset($element) && (stripos('sep',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Sep') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('oct',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthOct" value="oct" {{ isset($element) && (stripos('oct',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Oct') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('nov',$element->allowed_months) !== false) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthNov" value="nov" {{ isset($element) && (stripos('nov',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Nov') }}
                </td>
                <td class="cafeAllowedMonthTD {{ isset($element) && (stripos('dec',$element->allowed_months) !== false ) ? 'highlightedMonth' :'' }}">
                  <input class="cafeAllowedMonthCB hidden" type="checkbox" name="elemAllowedMonthDec" value="dec" {{ isset($element) && (stripos('dec',$element->allowed_months) !== false) ? 'checked' :'' }}>
                  {{ __('Dec') }}
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <button class="btn btn-primary" type="submit" name="saveElement">Save</button>
            <button class="btn btn-primary" type="button" onclick="window.location='{{ URL::previous() }}'" name="cancelManageElement">Cancel</button>
          </td>
        </tr>
      </form>
    </table>
  </div>
</div>
@endsection
