<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    public $timestamps = false;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'fullname', 'password','joining_date',
        'user_status', 'tax_id','perm_address',
        'temp_address', 'temp_unoff_address', 'daily_commute_from', 'commute_distance',
        'car_allowed','car_allowance_limit', 'licence_plate',
        'preferred_lang', 'roles','cafe_eligible_from',
        'cafe_eligible_until','cafe_yearly_limit',
        'cafe_emp_type','cafe_szep_leisure_pocket',
        'cafe_szep_accom_pocket','cafe_szep_dining_pocket',
        'cafe_planning_open'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/
    public function expenses(){
      return $this->hasMany('App\TravelExpense', 'username', 'username');
    }

}
