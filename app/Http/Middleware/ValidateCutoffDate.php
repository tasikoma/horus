<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\MessageBag;
use App\CutoffDates;

class ValidateCutoffDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request['coStart']);
        if(isset($request['coStart']) && isset($request['coEnd'])){
        $allEnd = CutoffDates::latest('end') -> first() ;
        //dd($allEnd -> end);
        if(strtotime($request['coStart']) >= strtotime($request['coEnd'])){
          $errors = new \Illuminate\Support\MessageBag();
          $errors->add('equal', 'This Start date is later than end date!');
          return  back() -> withInput() -> withErrors($errors);

        }
        if (strtotime($request['coStart']) <= strtotime($allEnd -> end)) {
          $errors = new \Illuminate\Support\MessageBag();

          // add your error messages:
          $errors->add('exist', 'This Date is already covered in the added cut-off dates!');

          return  back()-> withInput() -> withErrors($errors);
        }
        else{
          return $next($request);
        }
      }
      else{
        return $next($request);
      }
    }
}
