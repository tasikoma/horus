<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Horus') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>

      <script src="{{ asset('js/custom.js') }}" ></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <!-- center Side Of Navbar -->
                    <ul class="navbar-nav mc-auto">
                      <li class="nav-item dropdown">
                        <a  class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Admin <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('userman.create' ) }}">
                              {{ __('Add New User') }}
                          </a>
                            <a class="dropdown-item" href="{{ route('userman.index') }}">
                                {{ __('Search/Edit Users') }}
                            </a>
                        </div>
                      </li>
                      <li class="nav-item dropdown">
                        <a  class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Cafeteria <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('plan.index') }}">
                                {{ __('Cafeteri Declaration') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('elements.index') }}">
                                {{ __('Cafeteria elements') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('families.index') }}">
                                {{ __('Element Families') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('suppliers.index') }}">
                                {{ __('Suppliers') }}
                            </a>
                        </div>
                      </li>

                      <li class="nav-item dropdown">
                        <a  class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Travel Expense <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('tes.myht') }}">
                                {{ __('Home Travel') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('tes.mydc') }}">
                                {{ __('Daily Commute') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('admin.tes.index') }}">
                                {{ __('Report') }}
                            </a>
                            <a class="dropdown-item" href="/cutoffdates">
                                {{ __('Cutoff Date Management') }}
                            </a>
                        </div>
                      </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('myprofile' ) }}">
                                        {{ __('My Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                </div>
                            </li>



                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

</body>
</html>
