@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1>Cafeteria Element Family Management</h1>
  </div>
  @include('layouts.errordiv')
  <div class="container-fluid mc-auto bg-white shadow-sm">
  @foreach($families as $family)
    <table class="table .table-striped .table-hover" style="width:100%">
      <thead class="thead-dark">
        <tr>
          <th colspan="3">{{ __('Name of Family:  '.$family->name) }}</th>
          <th colspan="2"style="text-align:right;">
            <a href="{{ route('families.edit', $family->id) }}">
            <button class="btn btn-primary" type="button" name="editFamilyButton">
              <span class="glyphicon glyphicon-edit"></span>
            </button>
          </a>
          <form class="" action="{{ route('families.destroy', $family->id) }}" method="post" style="display:inline;">
            @csrf
            {{ method_field('DELETE') }}
            <button class="btn btn-primary" type="submit" name="disableFamilyButton">
              <span class="glyphicon glyphicon-trash"></span>
            </button>
          </form>
        </th>
      </tr>
      <tr>
          <th>{{ __('Common Tax: '.$family->common_tax .' %')  }}</th>
          <th>{{ __('Common yearly limit: '.$family->common_limit .' HUF') }}</th>
          <th>{{ __('Family yearly limit: '.$family->overall_limit .' HUF') }}</th>
          <th>{{ __('Mandatory to choose element? '. ($family->mandatory == 1 ? 'Yes':'No')) }}</th>
          <th>{{ __('Number of mandatory items: '.$family->num_of_mandatory ) }}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="5" style="text-align:center;">{{ __('Elements of the family:') }}</td>
      </tr>

      @foreach($family->children as $element)
        <tr>
          <td colspan="3">{{ $element->name }}</td>
          <td>{{ __('Tax: '.$element->tax .' %') }}</td>
          <td>{{ __('Yearly Limit:'.$element->yearly_limit) }}</td>
        </tr>
      @endforeach
    </tbody>
      <tfoot class="tfoot-dark">
        <tr>
          <td colspan="10" style="text-align:center;">

          </td>
        </tr>
      </tfoot>
    </table>
    @endforeach
    <a href="{{ route('families.create') }}">
      <button class="btn btn-primary"  type="button" name="addFamilyBtn">{{ __('Add New Family') }}</button>
    </a>
  </div>
</div>
@endsection
