<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class MyProfileController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('distance')->only(['update']);
    }

    public function index()
    {
        //

        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);

        $user = \App\User::where('id',auth()->id())->first();
        return view('myprofile', compact('user'));

    }

    public function edit()
    {
      $user = \App\User::where('id',auth()->id())->first();
      return view('editmyprofile', compact('user'));

    }

    public function update(Request $request)
    {
      $user = \App\User::where('id',auth()->id())->first();

      $km = session()->get('km');

      $permAddr = $request['permAddressCtry'].
          '__'.$request['permAddressZip'].
          '__'.$request['permAddressCity'].
          '__'.$request['permAddressStreet'].
          '__'.$request['permAddressNum'].
          '__'.$request['permAddressStairDoor'];


      $tempAddr = $request['tempAddressCtry'].
          '__'.$request['tempAddressZip'].
          '__'.$request['tempAddressCity'].
          '__'.$request['tempAddressStreet'].
          '__'.$request['tempAddressNum'].
          '__'.$request['tempAddressStairDoor'] ;


      $tempUOAddr = $request['tempUOAddressCtry'].
          '__'.$request['tempUOAddressZip'].
          '__'.$request['tempUOAddressCity'].
          '__'.$request['tempUOAddressStreet'].
          '__'.$request['tempUOAddressNum'].
          '__'.$request['tempUOAddressStairDoor'] ;


      $user -> user_status = 1 ;
      $user -> tax_id = encrypt($request['taxId']);
      

      if($request['permAddressCtry'] !== null){
        $user -> perm_address = encrypt($permAddr);
      }
      else{
        $user -> perm_address = null;
      }

      if($request['tempAddressCtry'] !== null){
        $user -> temp_address = encrypt($tempAddr);
      }
      else{
        $user -> temp_address = encrypt($tempAddr);
      }
      if($request['tempUOAddressCtry'] !== null){
        $user -> temp_unoff_address = encrypt($tempUOAddr);
      }
      else{
        $user -> temp_unoff_address = null;
      }


      $user -> daily_commute_from = $request['selectCommuteFrom'];
      $user -> commute_distance = $km ;
      $user -> licence_plate = encrypt($request['licensePlate']);
      $user -> preferred_lang = $request['selectPreferredLang'];

      $user-> save();

      return redirect('myprofile');
    }

}
