@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    @if($act == 'new')
      <h1>{{ __('Add New Supplier') }}</h1>
    @else
      <h1>{{ __('Edit Supplier') }}</h1>
    @endif
  </div>
  @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      @if($act == 'new')
      <form class="" action="{{ route('suppliers.store')}}" method="post">
      @else
      <form class="" action="{{ route('suppliers.update', $sup->id)}}" method="post">
        {{ method_field('PATCH') }}
      @endif
        @csrf
        <tr>
          <td>{{ __('Name:') }}</td>
          <td>
            <input type="text" name="supplierName" value="{{ isset($sup) ? $sup->name : '' }}" placeholder="{{ __('Name of the supplier') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Address:') }}</td>
          <td>
            <input type="text" name="supplierAddress" value="{{ isset($sup) ? $sup->address : '' }}" placeholder="{{ __('Address of supplier') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('E-mail:') }}</td>
          <td>
            <input type="text" name="supplierMail" value="{{ isset($sup) ? $sup->mail : '' }}" placeholder="{{ __('E-mail of supplier') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Phone:') }}</td>
          <td>
            <input type="text" name="supplierPhone" value="{{ isset($sup) ? $sup->phone : '' }}" placeholder="{{ __('Phone number of supplier') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Contact Person:') }}</td>
          <td>
            <input type="text" name="supplierContact" value="{{ isset($sup) ? $sup->contact_person : '' }}" placeholder="{{ __('Contact Person within the supplier') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Account:') }}</td>
          <td>
            <input type="text" name="supplierAccount" value="{{ isset($sup) ? $sup->account : '' }}" placeholder="{{ __('Financial account of supplier') }}">
          </td>
        </tr>
        <tr>
          <td>{{ __('Comments:') }}</td>
          <td>
            <textarea name="supplierComment" rows="3"  placeholder="{{ __('Any commnets about the supplier') }}">{{ isset($sup) ? $sup->comment : '' }}</textarea>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <button class="btn btn-primary" type="submit" name="saveSupplier">Save</button>
            <button class="btn btn-primary" type="button" name="cancelManageSupplier">Cancel</button>
          </td>
        </tr>
      </form>
    </table>
  </div>
</div>
@endsection
