@extends('layouts.homeLayout')
@section('content')

<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    @if($expType == 'ht')
      <h1 class="mc-auto">Add New Home Travel Expense Item</h1>
    @elseif($expType == 'dc')
      <h1 class="mc-auto">Add New Daily Commute Expense Item</h1>
    @else
      <h1 class="mc-auto">Error</h1>
    @endif

  </div>
  @include('layouts.errordiv')
  
  <div class="container mc-auto bg-white shadow-sm">
    <form class="" action="{{ route('tes.store') }}" method="post" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="expType" value="{{ $expType }}"/>
      <table class="table .table-striped .table-hover" style="width:100%">
        <tr>
          <td>{{ __('Travel Type')}}</td>
          <td>
            <select id="selectTT_id" class="" name="selectTravelType" onchange="traveltypereroute()">
              <option value="public" selected>{{ __('Public Transport')}}</option>
              @if($expType == 'dc')
                <option value="car">{{ __('Car')}}</option>
              @endif
            </select>
          </td>
        </tr>
        <div class="forPublic">
          <tr>
            <td>{{ __('Ticket Type')}}</td>
            <td>
              <select class="" name="selectTicketType">
                <option value="ticket" {{  old('selectTicketType') == 'ticket' ? 'selected':''}}>{{ __('Ticket')}}</option>
                @if($expType == 'ht')
                  <option value="planeticket" {{ old('selectTicketType') == 'planeticket' ? 'selected':''}}>{{ __('Plane Ticket')}}</option>
                @endif
                @if($expType == 'dc')
                  <option value="pass" {{ old('selectTicketType') == 'pass' ? 'selected':''}}>{{ __('Monthly pass')}}</option>
                @endif
              </select>
            </td>
          </tr>
          <tr>
            <td>{{ __('Date of Travel')}}</td>
            <td> <input type="date" name="dateOfTravel"  value="{{ old('dateOfTravel')}}"/> </td>
          </tr>
          <tr>
            <td>{{ __('From')}}</td>
            <td>
              <select class="" name="selectFrom">
                @if(auth()->user()->perm_address !== null)
                  <option value="perm" {{ old('selectFrom') == 'perm' ? 'selected':''}}>{{ str_replace('__' ,' ', decrypt(auth()->user()->perm_address)) }}</option>
                @endif
                @if(auth()->user()->temp_address !== null)
                  <option value="temp" {{ old('selectFrom') == 'temp' ? 'selected':''}}>{{ str_replace('__' ,' ', decrypt(auth()->user()->temp_address)) }}</option>
                @endif
                @if(auth()->user()->temp_unoff_address !== null)
                  <option value="tempUO" {{ old('selectFrom') == 'tempUO' ? 'selected':''}}>{{ str_replace('__' ,' ', decrypt(auth()->user()->temp_unoff_address)) }}</option>
                @endif
                @if($expType == 'dc')
                  <option value="office" {{ old('selectFrom') == 'office' ? 'selected':''}}>ABG Office</option>
                @endif
              </select>
            </td>
          </tr>
          <tr>
            <td>{{ __('To')}}</td>
            <td>
              <select class="" name="selectTo">
                @if(auth()->user()->perm_address !== null)
                  <option value="perm" {{ old('selectTo') == 'perm' ? 'selected':''}}>{{ str_replace('__' ,' ', decrypt(auth()->user()->perm_address)) }}</option>
                @endif
                @if(auth()->user()->temp_address !== null)
                  <option value="temp" {{ old('selectTo') == 'temp' ? 'selected':''}}>{{ str_replace('__' ,' ', decrypt(auth()->user()->temp_address)) }}</option>
                @endif
                @if(auth()->user()->temp_unoff_address !== null)
                  <option value="tempUO" {{ old('selectTo') == 'tempUO' ? 'selected':''}}>{{ str_replace('__' ,' ', decrypt(auth()->user()->temp_unoff_address)) }}</option>
                @endif
                @if($expType == 'dc')
                  <option value="office" {{ old('selectTo') == 'office' ? 'selected':''}}>ABG Office</option>
                @endif
              </select>
            </td>
          </tr>
          <tr>
            <td>{{ __('Gross Price')}}</td>
            <td> <input type="text" name="grossAmount" value="{{ old('grossAmount')}}"> </td>
          </tr>
          <tr>
            <td>{{ __('Calculated refound')}}</td>
            <td> <div id="calcRefound_cell"></div> </td>
          </tr>
          <tr>
            <td>{{ __('Documents of purchase')}}</td>
            <td>
              <div class="file-upload-wrapper">
                <input multiple="multiple" type="file" name="docs[]" required/>
              </div>
            </td>
          </tr>
        </div>


      </table>
        <button class="btn btn-primary" type="submit" name="button">Save</button>
        <button class="btn btn-primary" type="button" name="cancel">Cancel</button>
      </form>
    </div>

</div>

@endsection
