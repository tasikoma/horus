<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_expenses', function (Blueprint $table) {
          $table->bigIncrements('id')->unique();
          $table->string('username',50);
          $table->date('sub_date');
          $table->string('sub_month',20);
          $table->string('exp_type');
          $table->tinyInteger('exp_status');
          $table->string('travel_type', 20);
          $table->string('ticket_type', 20)->nullable();
          $table->date('travel_date');
          $table->string('travel_from', 20);
          $table->string('travel_to', 20);
          $table->bigInteger('exp_gross_amount')->nullable();
          $table->bigInteger('exp_distance')->nullable();
          $table->bigInteger('exp_refund');
          $table->string('doc_url1')->nullable();
          $table->string('doc_url2')->nullable();
          $table->string('reviewed_by',50)->nullable();
          $table->string('comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_expenses');
    }
}
