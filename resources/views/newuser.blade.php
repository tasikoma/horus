@extends('layouts.homeLayout')
@section('content')

<div class="container mc-auto bg-white shadow-sm">
  @if (isset($user))
    <h1>Edit User</h1>
  @else
  <h1>Add New User</h1>
  @endif
  @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">
    @if (!isset($user))
      <form  id="addNewUserForm" action="{{ route('userman.store') }}" method="post">
    @else
      <form  id="addNewUserForm" action="{{ route('userman.update', $user->id ) }}" method="post">
        {{ method_field('PATCH') }}
    @endif
      @csrf
    <table class="table .table-striped .table-hover" style="width:100%">
      <tr>
        <td>{{ __('Company ID')}}</td>
        <td> <input type="text" name="companyId" value="{{ isset($user) ? $user->username : ''}}" /> </td>
      </tr>
      <tr>
        <td>{{ __('Full Name')}}</td>
        <td><input type="text" name="fullName" value="{{ isset($user) ? decrypt($user->fullname) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('E-mail')}}</td>
        <td><input type="text" name="email" value="{{  isset($user) ? $user->email : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('Joining Date')}}</td>
        <td><input type="date" name="joiningDate" value="{{  isset($user) ? decrypt($user->joining_date) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('Termination Date')}}</td>
        <td><input type="date" name="terminationDate" value="{{ ( isset($user) && $user->termination_date !== null ) ? decrypt($user->termination_date) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('Role')}}</td>
        <td>
          <select class="" name="selectUserRole">
            <option value="user" {{ isset($user) && decrypt($user->roles) == 'user' ? 'selected' : ''}}>User</option>
            <option value="admin" {{ isset($user) && decrypt($user->roles) == 'admin' ? 'selected' : ''}}>Administrator</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>{{ __('Tax ID')}}</td>
        <td><input type="text" name="taxId" value="{{ isset($user->tax_id) ? decrypt($user->tax_id) : ''}}"/></td>
      </tr>

      <tr>
        <td>{{ __('Permanent address')}}</td>
        <?php
          if(isset($user) && $user->perm_address !== null){
            $perm = explode('__' , decrypt($user->perm_address));
          }
        ?>
        <td>
          <input type="text" name="permAddressCtry"  placeholder="Country" value="{{ isset($perm[0]) ? $perm[0] : ''}}"/>
          <input type="text" name="permAddressZip"  placeholder="Zip Code" value="{{ isset($perm[1]) ? $perm[1] : ''}}"/>
          <input type="text" name="permAddressCity"  placeholder="City" value="{{ isset($perm[2]) ? $perm[2] : ''}}"/>
          <input type="text" name="permAddressStreet"  placeholder="Street" value="{{ isset($perm[3]) ? $perm[3] : ''}}"/>
          <input type="text" name="permAddressNum"  placeholder="Number" value="{{ isset($perm[4]) ? $perm[4] : ''}}"/>
          <input type="text" name="permAddressStairDoor" placeholder="Stairs and Door" value="{{ isset($perm[5]) ? $perm[5] : ''}}"/>
        </td>
      </tr>

      <tr>
        <td>{{ __('Temporary Address')}}</td>
        <?php
          if(isset($user) && $user->temp_address !== null){
            $temp = explode('__' , decrypt($user->temp_address));
          }
        ?>
        <td>
          <input type="text" name="tempAddressCtry"  placeholder="Country" value="{{ isset($temp[0]) ? $temp[0] : ''}}"/>
          <input type="text" name="tempAddressZip"  placeholder="Zip Code" value="{{ isset($temp[1]) ? $temp[1] : ''}}"/>
          <input type="text" name="tempAddressCity"  placeholder="City" value="{{ isset($temp[2]) ? $temp[2] : ''}}"/>
          <input type="text" name="tempAddressStreet"  placeholder="Street" value="{{ isset($temp[3]) ? $temp[3] : ''}}"/>
          <input type="text" name="tempAddressNum"  placeholder="Number" value="{{ isset($temp[4]) ? $temp[4] : ''}}"/>
          <input type="text" name="tempAddressStairDoor" placeholder="Stairs and Door" value="{{ isset($temp[5]) ? $temp[5] : ''}}"/>
        </td>
      </tr>
      <tr>
        <td>{{ __('Unofficcial Temporary Address')}}</td>
        <?php
          if(isset($user) && $user->temp_unoff_address !== null){
            $tempUO = explode('__' , decrypt($user->temp_unoff_address));
          }
        ?>
        <td>
          <input type="text" name="tempUOAddressCtry"  placeholder="Country" value="{{ isset($tempUO[0]) ? $tempUO[0] : ''}}"/>
          <input type="text" name="tempUOAddressZip"  placeholder="Zip Code" value="{{ isset($tempUO[1]) ? $tempUO[1] : ''}}"/>
          <input type="text" name="tempUOAddressCity"  placeholder="City" value="{{ isset($tempUO[2]) ? $tempUO[2] : ''}}"/>
          <input type="text" name="tempUOAddressStreet"  placeholder="Street" value="{{ isset($tempUO[3]) ? $tempUO[3] : ''}}"/>
          <input type="text" name="tempUOAddressNum"  placeholder="Number" value="{{ isset($tempUO[4]) ? $tempUO[4] : ''}}"/>
          <input type="text" name="tempUOAddressStairDoor" placeholder="Stairs and Door" value="{{ isset($tempUO[5]) ? $tempUO[5] : ''}}"/>
        </td>
      </tr>
      <tr>
        <td>{{ __('Commuting Daily from')}}</td>
        <td>
          <select class="" name="selectCommuteFrom">
            <option value="perm" {{ isset($user) && $user->daily_commute_from == 'perm' ? 'selected' : ''}} >
              {{ __('Permanent Address')}}
            </option>
            <option value="temp" {{ isset($user) && $user->daily_commute_from == 'temp' ? 'selected' : ''}}>
              {{ __('Temporary Address')}}
            </option>
            <option value="tempUO" {{ isset($user) && $user->daily_commute_from == 'tempUO' ? 'selected' : ''}}>
              {{ __('Temporary Address (unofficcial)')}}
            </option>
          </select>
        </td>
      </tr>
      <tr>
        <td>{{ __('Commute Distance (km)')}}</td>
        <td>{{ isset($user->commute_distance)  ? $user->commute_distance : 'N/A'}}</td>
      </tr>
      <tr>
        <td>{{ __('Car Commute Allowed') }}</td>
        <td><input type="checkbox" name="carAllowed" value="1" {{ isset($user) && $user->car_allowed == 1 ? 'checked' : '' }} /></td>

      <tr>
        <td>{{ __('Car Allowance')}}</td>
        <td><input type="text" name="carAllowanceLimit" value="{{ isset($user) && $user->car_allowance_limit != null ? $user->car_allowance_limit : '' }}" /></td>
      </tr>
      <tr>
        <td>{{ __('License Plate')}}</td>
        <td><input type="text" name="licensePlate" value="{{ isset($user) && $user->licence_plate != null ? decrypt($user->licence_plate) : '' }}" /></td>
      </tr>

      <tr>
        <td>{{ __('Preferred Language')}}</td>
        <td><select class="" name="selectPreferredLang">
          <option value="en" {{ isset($user) && $user->preferred_lang == 'en' ? 'selected' : ''}}>English</option>
          <option value="hu" {{ isset($user) && $user->preferred_lang == 'hu' ? 'selected' : ''}}>Magyar</option>
        </select> </td>
      </tr>
      <tr>
        <td>{{ __('Employee type:')}}</td>
        <td>
          <select class="" name="selectEmployeeType">
            <option value="full_perm">Full time,Permanent</option>
            <option value="full_cont">Full time,Contractor</option>
            <option value="part_perm">Part time,Permanent</option>
            <option value="part_cont">Part time,Contractor</option>
            <option value="dual">Dual Education</option>
            <option value="student">Student</option>
          </select>
        </td>
      </tr>

      @if(isset($user))
      <tr>
        <td colspan="2">{{ __('Cafetera Related Data')}}</td>
      </tr>
      <tr>
        <td>{{ __('Eligible for Cafeteria from:')}}</td>
        <td><input type="date" name="cafeEligibleFrom" value="{{  isset($user->cafe_eligible_from) ? $user->cafe_eligible_from : '' }}"/></td>
      </tr>
      <tr>
        <td>{{ __('Eligible for Cafeteria until:')}}</td>
        <td>
          <input type="date" name="cafeEligibleUntil" value="{{  isset($user->cafe_eligible_until) ? $user->cafe_eligible_until : ''}}"/>
        </td>
      </tr>
      <tr>
        <td>{{ __('Cafeteria yearly limit (calculated):')}}</td>
        <td><input type="text" name="cafeEligibleLimit" value="{{  isset($user->cafe_yearly_limit) ? $user->cafe_yearly_limit : ''}}"/></td>
      </tr>

      <tr>
        <td>{{ __('OTP Szép Card - Dining pocket:')}}</td>
        <td><input type="text" name="cafeSzepDining" value="{{  isset($user->cafe_szep_dining_pocket) && $user->cafe_szep_dining_pocket != null ? decrypt($user->cafe_szep_dining_pocket) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('OTP Szép Card - Leisure pocket:')}}</td>
        <td><input type="text" name="cafeSzepLeisure" value="{{  isset($user->cafe_szep_leisure_pocket) && $user->cafe_szep_leisure_pocket != null  ? decrypt($user->cafe_szep_leisure_pocket) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('OTP Szép Card - Accomodation pocket:')}}</td>
        <td><input type="text" name="cafeSzepAccom" value="{{  isset($user->cafe_szep_accom_pocket) && $user->cafe_szep_accom_pocket != null  ? decrypt($user->cafe_szep_accom_pocket) : ''}}"/></td>
      </tr>
      <tr>
        <td>{{ __('User open for declaration:')}}</td>
        <td><input type="checkbox" name="cafeDeclOpen" value="1" {{ isset($user) && $user->cafe_planning_open == 1 ? 'checked' : '' }} /></td>
      </tr>
      @endif
    </table>
    <button class="btn btn-primary" type="submit" name="button" >Save</button> </a>
    <button class="btn btn-primary" type="button" onclick="window.location='{{ URL::previous() }}'" name="cancelManageElement">Cancel</button>
  </form>

  </div>

</div>

@endsection
