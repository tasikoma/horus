<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CafeElement extends Model
{
  public function supplier()
  {
      return $this->belongsTo('App\CafeSupplier');
  }

  public function family()
  {
      return $this->belongsTo('App\CafeFamily','family_id','id');
  }

  protected $guarded = ['id'];
}
