@extends('layouts.homeLayout')
@section('content')
  <div class="container mc-auto bg-white shadow-sm">
    <h1>My profile</h1>
  </div>

  <div class="container mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      <tr>
        <td>{{ __('Company ID')}}</td>
        <td>{{ $user->username }}</td>
      </tr>
      <tr>
        <td>{{ __('Full Name')}}</td>
        <td>{{ decrypt($user->fullname) }}</td>
      </tr>
      <tr>
        <td>{{ __('E-mail')}}</td>
        <td>{{ $user->email }}</td>
      </tr>
      <tr>
        <td>{{ __('Joining Date')}}</td>
        <td>{{ decrypt($user->joining_date) }}</td>
      </tr>

      <tr>
        <td>{{ __('Account status')}}</td>
        @if($user->user_status === 0)
          <td>Active, but personal data missing</td>
        @elseif($user->user_status === 1)
          <td>Active</td>
        @else
          <td>Disabled</td>
        @endif
      </tr>
      <tr>
        <td>{{ __('Tax ID')}}</td>
        <td>{{ decrypt($user->tax_id) }}</td>
      </tr>
      <tr>
        <td>{{ __('Permanent address')}}</td>
        <td>{{ isset($user->perm_address) ? str_replace('_',' ' , decrypt($user->perm_address)) : ''}}</td>
      </tr>
      <tr>
        <td>{{ __('Temporary Address')}}</td>
        <td>{{ isset($user->temp_address) ? str_replace('_',' ' ,decrypt($user->temp_address)) : '' }}</td>
      </tr>
      <tr>
        <td>{{ __('Unofficial Temporary Address')}}</td>
        <td>{{ isset($user->temp_unoff_address) ? str_replace('_',' ' ,decrypt($user->temp_unoff_address)) : '' }}</td>
      </tr>
      <tr>
        <td>{{ __('Commuting Daily from')}}</td>
        @if($user->daily_commute_from == 'perm')
          <td> {{ __('Permanent Address')}}</td>
        @elseif($user->daily_commute_from == 'temp')
          <td> {{ __('Temporary Address')}}</td>
        @else
          <td> {{ __('Temporary Unofficial Address')}}</td>
        @endif
      </tr>
      <tr>
        <td>{{ __('Commute Distance (km)')}}</td>
        <td>{{ isset($user->commute_distance)  ? $user->commute_distance : 'N/A'}}</td>
      </tr>
      <tr>
        <td>{{ __('Car Commute Allowed')}}</td>
        @if($user->car_allowed === 0 || $user->car_allowed === null)
          <td>No</td>
        @else
          <td>Yes</td>
        @endif
      </tr>
      <tr>
        <td>{{ __('Car Allowance')}}</td>
        <td>{{ $user->car_allowance_limit }}</td>
      </tr>
      <tr>
        <td>{{ __('License Plate')}}</td>
        <td>{{ decrypt($user->licence_plate) }}</td>
      </tr>
      <tr>
        <td>{{ __('Preferred Language')}}</td>
        <td>{{ $user->preferred_lang }}</td>
      </tr>
    </table>
    <a href="{{ route('myprofile.edit') }}"> <button class="btn btn-primary" type="button" name="button">Edit my profile</button> </a>
  </div>



@endsection
