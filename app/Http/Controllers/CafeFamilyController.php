<?php

namespace App\Http\Controllers;

use App\CafeFamily;
use App\CafeElement;
use Illuminate\Http\Request;

class CafeFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $families = CafeFamily::all();

      return view('cafe.families',compact('families'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $act = 'new';
      $elements = CafeElement::where('status',1)->get();
      return view('cafe.managefamilies', compact('act','elements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      request()->validate([
        'familyName' => 'required|string|max:191',
        'familyCommonTax' => 'numeric|nullable',
        'familyCommonLimit' => 'numeric|nullable',
        'familyOverallLimit' => 'numeric|nullable',
        'familyMandatory' => 'numeric|nullable',
        'familyOverallLimit' => 'numeric|nullable',

      ]);

      $created = CafeFamily::create([
        'name' => $request['familyName'],
        'status' => 1,
        'common_tax' => $request['familyCommonTax'],
        'common_limit' => $request['familyCommonLimit'],
        'overall_limit' => $request['familyOverallLimit'],
        'mandatory' => $request['familyMandatory'] ,
        'num_of_mandatory' => $request['familyMandatoryNum'] ,

      ]);

      //update the elements with family id
      $inputs = $request->toArray();
      $els = [];

      foreach ($inputs as $key => $value) {

        if(stripos($key,'addElement_') !== false){
          $els[]=$value;
        }
      }
      foreach($els as $el){
        $element = CafeElement::find($el);
        $element -> family_id = $created->id ;
        $element -> save();
      }
      return redirect( route('families.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CafeFamily  $cafeFamily
     * @return \Illuminate\Http\Response
     */
    public function show(CafeFamily $family)
    {
        return redirect( route('families.index') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CafeFamily  $cafeFamily
     * @return \Illuminate\Http\Response
     */
    public function edit(CafeFamily $family)
    {
      $act = 'edit';
      $elements = CafeElement::where('status',1)->get();
      return view('cafe.managefamilies', compact('act', 'family','elements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CafeFamily  $cafeFamily
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CafeFamily $family)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CafeFamily  $cafeFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(CafeFamily $family)
    {
      CafeFamily::destroy($family->id);
      return redirect()->back();
    }
}
