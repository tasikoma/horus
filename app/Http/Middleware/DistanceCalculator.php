<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class DistanceCalculator
{

    protected function getkm($adr){
      $adr = urlencode($adr);
      $url =  'https://geocoder.api.here.com/6.2/geocode.json?searchtext='.$adr.'&responseattributes=none&locationattributes=none&app_id='.env('HERE_API_ID').'&app_code='.env('HERE_API_CODE').'&gen=9' ;

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
      curl_setopt($ch, CURLOPT_URL, $url );
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      //temp stuff
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      //dd($ch);
      $result = curl_exec($ch);

      curl_close($ch);
      $result = json_decode($result,TRUE);

      $coord = $result['Response']['View'][0]['Result'][0]['Location']['NavigationPosition'][0];
      $lat = $coord['Latitude'];
      $lon = $coord['Longitude'];

      //getting km from here app_id
      $hereUrl = 'https://route.api.here.com/routing/7.2/calculateroute.json?app_id='.env('HERE_API_ID').'&app_code='.env('HERE_API_CODE').'&waypoint0=geo!'.$lat.','.$lon.'&waypoint1=geo!47.52071,19.06448&mode=fastest;car;traffic:disabled&routeattributes=sm';
      $hch = curl_init();


      curl_setopt($hch, CURLOPT_HTTPPROXYTUNNEL, 0);
      curl_setopt($hch, CURLOPT_URL, $hereUrl );
      curl_setopt($hch, CURLOPT_CUSTOMREQUEST, 'GET');
      curl_setopt($hch, CURLOPT_RETURNTRANSFER, TRUE);
      //temp stuff
      curl_setopt($hch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($hch, CURLOPT_SSL_VERIFYPEER, 0);
      $result = curl_exec($hch);
      curl_close($hch);
      //echo $result;
      $result = json_decode($result,TRUE);
      //print_r($result);
      $km =  $result['response']['route'][0]['summary']['distance'];
      $km = ceil($km / 1000);

      return $km;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
      //check if address changed
      //$user = \App\User::where('id',auth()->id())->first();

      if($request['permAddressCtry'] != null && $request['selectCommuteFrom'] == 'perm'){
        $adr['perm'] = $request['permAddressCtry'].
            ' '.$request['permAddressZip'].
            ' '.$request['permAddressCity'].
            ' '.$request['permAddressStreet'].
            ' '.$request['permAddressNum'] ;
            $km = DistanceCalculator::getkm($adr['perm']);
      }
      if($request['tempAddressCtry'] != null && $request['selectCommuteFrom'] == 'temp'){
        $adr['temp'] = $request['tempAddressCtry'].
            ' '.$request['tempAddressZip'].
            ' '.$request['tempAddressCity'].
            ' '.$request['tempAddressStreet'].
            ' '.$request['tempAddressNum'];
            $km = DistanceCalculator::getkm($adr['temp']);
      }
      if($request['tempUOAddressCtry'] != null && $request['selectCommuteFrom'] == 'tempUO'){
        $adr['tempUO'] = $request['tempUOAddressCtry'].
            ' '.$request['tempUOAddressZip'].
            ' '.$request['tempUOAddressCity'].
            ' '.$request['tempUOAddressStreet'].
            ' '.$request['tempUOAddressNum'];
            $km = DistanceCalculator::getkm($adr['tempUO']);
      }

      if(!isset($km)){
        $km = null;
      }

      $request -> session()->flash('km' , $km );
      return $next($request);
    }
}
