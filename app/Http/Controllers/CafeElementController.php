<?php

namespace App\Http\Controllers;

use App\CafeElement;
use App\CafeSupplier ;
use App\CafeFamily ;
use Illuminate\Http\Request;

class CafeElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $elements = CafeElement::all();

      return view('cafe.elements',compact('elements'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $act = 'new';
      $sups = CafeSupplier::where('status',1)->get();
      $fams = CafeFamily::where('status',1)->get();

      return view('cafe.manageelement', compact('act','sups','fams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      request()->validate([
        'elementName' => 'required|string|max:191',
        'elementSelectSupplier' => 'required|integer|exists:cafe_suppliers,id',
        'elementDescription' => 'required|max:255',
        'elementMLimit' => 'required|numeric',
        'elementFix' => 'numeric',
        'elementTax' => 'required|numeric',
        'elementBreachTax' => 'required|numeric',
        'elementMandatory' => 'boolean|nullable',
        'elementNeedsFallback' => 'boolean|nullable',
      ]);
      //get months
      $mon = $request->toArray();
      $months = [];
      foreach ($mon as $key => $value) {
        if(stripos($key,'elemAllowedMonth') !== false){
          $months[]=$value;
        }
      }
      if(in_array('all',$months)){
        $monthsFinal = 'all';
      }
      else{
        if(empty($months)){
          $monthsFinal = 'all';
        }
        else{
          $monthsFinal = implode(',',$months);
        }
      }

      CafeElement::create([
        'name' => $request['elementName'],
        'family_id' => $request['elementSelectFamily'] ,
        'supplier_id' => $request['elementSelectSupplier'],
        'description' => $request['elementDescription'],
        'monthly_limit' => $request['elementMLimit'],
        'fix_amount' => $request['elementFix'],
        'limit_breachable' => $request['elementLimitBrachable'],
        'multiplier' => $request['elementMultiplier'],
        'tax' => $request['elementTax'],
        'breachtax' => $request['elementBreachTax'] ,
        'status' => 1 ,
        'is_mandatory' => $request['elementMandatory'] ,
        'needs_fallback' => $request['elementNeedsFallback'] ,
        'fund_source' => $request['selectElementFund'],
        'allowed_months' => $monthsFinal,
        'added_by' => auth()->user()->username ,
      ]);

      return redirect( route('elements.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CafeElement  $cafeElement
     * @return \Illuminate\Http\Response
     */
    public function show(CafeElement $element)
    {
        return redirect( route('elements.index') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CafeElement  $cafeElement
     * @return \Illuminate\Http\Response
     */
    public function edit(CafeElement $element)
    {
      $act = 'edit';
      $sups = CafeSupplier::where('status',1)->get();
      $fams = CafeFamily::where('status',1)->get();
      return view('cafe.manageelement', compact('act', 'element','sups','fams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CafeElement  $cafeElement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CafeElement $element)
    {
      request()->validate([
        'elementName' => 'required|string|max:191',
        'elementSelectSupplier' => 'required|integer|exists:cafe_suppliers,id',
        'elementDescription' => 'required|max:255',
        'elementMLimit' => 'required|numeric',
        'elementFix' => 'numeric',
        'elementTax' => 'required|numeric',
        'elementBreachTax' => 'required|numeric',
        'elementMandatory' => 'boolean|nullable',
        'elementNeedsFallback' => 'boolean|nullable',
      ]);
      $original =   $element->getOriginal();
      $original['name'] !== $request['elementName'] ? $element -> name = $request['elementName'] : '';
      $original['family_id'] !== $request['elementSelectFamily'] ? $element -> family_id = $request['elementSelectFamily'] : '';
      $original['supplier_id'] !== $request['elementSelectSupplier'] ? $element -> supplier_id = $request['elementSelectSupplier'] : '';
      $original['description'] !== $request['elementDescription'] ? $element -> description = $request['elementDescription'] : '';
      $original['monthly_limit'] !== $request['elementMLimit'] ? $element -> monthly_limit = $request['elementMLimit'] : '';
      $original['fix_amount'] !== $request['elementFix'] ? $element -> fix_amount = $request['elementFix'] : '';
      $original['limit_breachable'] !== $request['elementLimitBrachable'] ? $element -> limit_breachable = $request['elementLimitBrachable'] : '';
      $original['multiplier'] !== $request['elementMultiplier'] ? $element -> multiplier = $request['elementMultiplier'] : '';
      $original['tax'] !== $request['elementTax'] ? $element -> tax = $request['elementTax'] : '';
      $original['breachtax'] !== $request['elementBreachTax'] ? $element -> breachtax = $request['elementBreachTax'] : '';
      $original['yearly_limit'] !== $request['elementYLimit'] ? $element -> yearly_limit = $request['elementYLimit'] : '';
      $original['is_mandatory'] !== $request['elementMandatory'] ? $element -> is_mandatory = $request['elementYLimit'] : '';
      $original['needs_fallback'] !== $request['elementNeedsFallback'] ? $element -> needs_fallback = $request['elementNeedsFallback'] : '';
      $original['fund_source'] !== $request['selectElementFund'] ? $element -> fund_source = $request['selectElementFund'] : '';

      $mon = $request->toArray();
      $months = [];
      foreach ($mon as $key => $value) {
        if(stripos($key,'elemAllowedMonth') !== false){
          $months[]=$value;
        }
      }
      if(in_array('all',$months)){
        $monthsFinal = 'all';
      }
      else{
        if(empty($months)){
          $monthsFinal = 'all';
        }
        else{
          $monthsFinal = implode(',',$months);
        }
      }

      $original['allowed_months'] !== $monthsFinal ? $element -> allowed_months = $monthsFinal : '';

      $element -> save();

      return redirect( route('elements.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CafeElement  $cafeElement
     * @return \Illuminate\Http\Response
     */
    public function destroy(CafeElement $element)
    {
      CafeElement::destroy($element->id);
      return redirect()->back();
    }
}
