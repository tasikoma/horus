@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <h1>User Management</h1>
</div>
<div class="filters container mc-auto bg-white shadow-sm">
  <form id="searchUserForm" class="" action="{{ route('userman.search') }}" method="post">
    @csrf
    <input type="text" name="searchUserField" placeholder="name,username,e-mail"/>
    <select class="" name="selectUserStatus">
      <option value="9" selected>All</option>
      <option value="0">Active, but personal data missing </option>
      <option value="1">Active</option>
      <option value="2">Disabled</option>
    </select>
    <button class="btn btn-primary" type="button" name="button" onclick="document.getElementById('searchUserForm').submit();">
      Search
    </button>
  </form>

</div>
<div class="list container mc-auto bg-white shadow-sm">
  <table  class="table .table-striped .table-hover" style="width:100%">
    @foreach ($users as $user)
    <tr>
      <td>{{ $user -> username }}</td>
      <td>{{ decrypt($user -> fullname ) }}</td>
      <td>{{ $user -> email }}</td>
      <td>
        <a href="/userman/{{ $user->id }}/edit">
          <button type="button" class="btn btn-primary btn-sm">
            <span class="glyphicon glyphicon-edit"></span> Edit
          </button>
        </a>
        <a href="/userman/{{ $user->id }}/edit">
          <button type="button" class="btn btn-primary btn-sm">
            <span class="glyphicon glyphicon-edit"></span> PW Reset
          </button>
        </a>
        <form style="display:inline;" action="{{ route('userman.destroy', $user->id ) }}" method="post">
          {{ method_field('DELETE') }}
          @csrf
          <button type="submit" class="btn btn-primary btn-sm" >
            <span class="glyphicon glyphicon-trash"></span> Delete
          </button>
        </form>

      </td>
    </tr>
    @endforeach
  </table>

</div>
{{ $users->links() }}

@endsection
