@extends('layouts.homeLayout')
@section('content')
<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1>My Cafeteria Declaration - {{ date('Y') }}</h1>
  </div>
  <div class="container">
    <div class="row">
      <div class="cafeElements col-2">
        <table class="cafeElementsTable" style="width:100%;">
          <thead>
            <tr>
              <th>Available elements</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Szép kárty - vendéglátás</td>
            </tr>
            <tr>
              <td>szép kártya - szállás</td>
            </tr>
            <tr>
              <td>szép kárty - szabadidő</td>
            </tr>
          </tbody>

        </table>
      </div>
      <div class="cafeplan col-10">
        <table class="cafePlanTable" style="width:100%;">
          <thead>
            <tr>
              <th>Chosen Elements</th>
            </tr>
            <tr>
              <th>Element name</th>
              <th>Jan</th>
              <th>Feb</th>
              <th>Mar</th>
              <th>Apr</th>
              <th>May</th>
              <th>Jun</th>
              <th>Jul</th>
              <th>Aug</th>
              <th>Sept</th>
              <th>Oct</th>
              <th>Nov</th>
              <th>Dec</th>
              <th>Sum</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>ezegyhosszúnév</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>nagyonsok</td>
            </tr>
            <tr>
              <td>ezegyhosszúnév</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>nagyonsok</td>
            </tr>
            <tr>
              <td>ezegyhosszúnév</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>nagyonsok</td>
            </tr>
            <tr>
              <td>ezegyhosszúnév</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>nagyonsok</td>
            </tr>
            <tr>
              <td>ezegyhosszúnév</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>1200</td>
              <td>nagyonsok</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
