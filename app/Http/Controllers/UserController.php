<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('distance') -> only('update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //

        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);
        $users = DB::table('users')->paginate(25);
        return view('usermanagement', ['users' => $users]);
    }

    public function search(Request $request)
    {
        //
        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);
        //validate searchoptions
        request()->validate([
          'searchUserField' => 'alpha_num|max:191',
          'selectUserStatus' => 'numeric|max:9',
        ]);
        if($request['selectUserStatus'] === 9){
          $users = \App\User::where('username','like','%'.$request['searchUserField'].'%') -> paginate(25);
        }
        else{
          if($request['selectUserStatus'] <= 2){
            $users = \App\User::where('username','like','%'.$request['searchUserField'].'%')
              -> where('user_status','=', $request['selectUserStatus'] )
              -> paginate(25);
          }
          else{
            $users = \App\User::where('username','like','%'.$request['searchUserField'].'%')
              -> where('user_status','<=', $request['selectUserStatus'] )
              -> paginate(25);
          }
        }

        return view('usermanagement', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);

        return view('newuser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);
        //validation

        request()->validate([
          'companyId' => 'required|alpha_num|max:191',
          'fullName' => 'required|string|max:191',
          'email' => 'required|email',
          'joiningDate' => 'required|date_format:Y-m-d',
          'terminationDate' => 'nullable|date_format:Y-m-d',
          'selectUserRole' => 'required|in:user,admin',
          'permAddressCtry' => 'nullable|alpha_dash',
          'permAddressZip' => 'nullable|alpha_dash',
          'permAddressCity' => 'nullable|alpha_dash',
          'permAddressStreet' => 'nullable|alpha_dash',
          'permAddressNum' => 'nullable|alpha_dash',
          'permAddressStairDoor' => 'nullable|alpha_dash',
          'tempAddressCtry' => 'nullable|alpha_dash',
          'tempAddressZip' => 'nullable|alpha_dash',
          'tempAddressCity' => 'nullable|alpha_dash',
          'tempAddressStreet' => 'nullable|alpha_dash',
          'tempAddressNum' => 'nullable|alpha_dash',
          'tempAddressStairDoor' => 'nullable|alpha_dash',
          'tempUOAddressCtry' => 'nullable|alpha_dash',
          'tempUOAddressZip' => 'nullable|alpha_dash',
          'tempUOAddressCity' => 'nullable|alpha_dash',
          'tempUOAddressStreet' => 'nullable|alpha_dash',
          'tempUOAddressNum' => 'nullable|alpha_dash',
          'tempUOAddressStairDoor' => 'nullable|alpha_dash',
          'selectCommuteFrom' => 'nullable|in:perm,temp,tempUO',
          'carAllowed' => 'nullable|in:0,1',
          'carAllowanceLimit' => 'nullable|numeric',
          'licensePlate' => 'nullable|alpha_dash',
          'cafeEligibleFrom' => 'nullable|date',
          'cafeEligibleUntil' => 'nullable|date',
          'cafeEligibleLimit' => 'numeric|nullable',
          'selectEmployeeType' => 'nullable|in:full_perm,full_cont,part_perm,part_cont,dual,student',
          'cafeSzepDining' => 'nullable|numeric',
          'cafeSzepLeisure' => 'nullable|numeric',
          'cafeSzepAccom' => 'nullable|numeric',
          'cafeDeclOpen' => 'nullable|in:0,1',
        ]);
        // middleware for API calls


        // link address with __
        if($request['permAddressCity'] != null && $request['tempAddressStreet'] != null){
        $permAddr = $request['permAddressCtry'].
            '__'.$request['permAddressZip'].
            '__'.$request['permAddressCity'].
            '__'.$request['permAddressStreet'].
            '__'.$request['permAddressNum'].
            '__'.$request['permAddressStairDoor'] ;
        }
        else{
          $permAddr = null ;
        }
        if($request['tempAddressCity'] != null && $request['tempAddressStreet'] != null){
        $tempAddr = $request['tempAddressCtry'].
            '__'.$request['tempAddressZip'].
            '__'.$request['tempAddressCity'].
            '__'.$request['tempAddressStreet'].
            '__'.$request['tempAddressNum'].
            '__'.$request['tempAddressStairDoor'] ;
        }
        else{
          $tempAddr = null ;
        }
        if($request['tempUOAddressCity'] != null && $request['tempUOAddressStreet'] != null){
        $tempUOAddr = $request['tempUOAddressCtry'].
            '__'.$request['tempUOAddressZip'].
            '__'.$request['tempUOAddressCity'].
            '__'.$request['tempUOAddressStreet'].
            '__'.$request['tempUOAddressNum'].
            '__'.$request['tempUOAddressStairDoor'] ;
        }
        else{
          $tempUOAddr = null ;
        }

        //calculate eligibility and limit by starting date
        $eligstart = strtotime('+ 2 months',strtotime($request['joiningDate']));
        $eligstart = strtotime('+ 1 days',$eligstart);

        //calc limit
        if(date('Y',$eligstart) >= date('Y')){
          $diff = (mktime(0,0,0,12,31,date('Y',$eligstart)) - $eligstart) / 60/60/24 ;
          $lim = 384000 * ($diff/365);
        }
        else{
          $lim = 384000;
        }

        User::create([
          'username' => $request['companyId'],
          'email' => $request['email'],
          'fullname' => encrypt($request['fullName']),
          'password' => Hash::make('FGrt1236'),
          'joining_date' => encrypt($request['joiningDate']),
          'termination_date' => encrypt($request['terminationDate']),
          'user_status' => 0 ,
          'tax_id' => $request['taxId'] != null ? encrypt($request['taxId']) : null,
          'perm_address' => $permAddr != null ? encrypt($permAddr) : null,
          'temp_address' => $tempAddr != null ? encrypt($tempAddr) : null,
          'temp_unoff_address' => $tempUOAddr != null ? encrypt($tempUOAddr) : null,
          'daily_commute_from' => $request['selectCommuteFrom'],
          'car_allowed' => $request['carAllowed'] == 1 ? '1' : '0',
          'car_allowance_limit' => $request['carAllowanceLimit'],
          'licence_plate' =>  $request['licensePlate'] != null ? encrypt($request['licensePlate']) : null,
          'preferred_lang' => $request['preferredLang'],
          'roles' => encrypt($request['selectUserRole']),
          'cafe_eligible_from' => date('Y-m-d',$eligstart),
          'cafe_eligible_until' => null,
          'cafe_yearly_limit'  => $lim,
          'cafe_emp_type'  => encrypt($request['selectEmployeeType']),
          'cafe_szep_dining_pocket'  => $request['cafeSzepDining'] != null ? encrypt($request['cafeSzepDining']) : null,
          'cafe_szep_leisure_pocket'  => $request['cafeSzepLeisure'] != null ? encrypt($request['cafeSzepLeisure']) : null,
          'cafe_szep_accom_pocket'  => $request['cafeSzepAccom'] != null ? encrypt($request['cafeSzepAccom']) : null,
          'cafe_planning_open' => $request['cafeDeclOpen'] == 1 ? '1' : '0',

        ]);

        return redirect('userman');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        $user = \App\User::where('id',auth()->id())->first();
        return view('myprofile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$id = $user->id;
        $user = \App\User::where('id', $id)->first();

        return view('newuser', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        request()->validate([
          'companyId' => 'required|alpha_num|max:191',
          'fullName' => 'required|string|max:191',
          'email' => 'required|email',
          'joiningDate' => 'required|date_format:Y-m-d',
          'terminationDate' => 'nullable|date_format:Y-m-d',
          'selectUserRole' => 'required|in:user,admin',
          'permAddressCtry' => 'nullable|alpha_dash',
          'permAddressZip' => 'nullable|alpha_dash',
          'permAddressCity' => 'nullable|alpha_dash',
          'permAddressStreet' => 'nullable|alpha_dash',
          'permAddressNum' => 'nullable|alpha_dash',
          'permAddressStairDoor' => 'nullable|alpha_dash',
          'tempAddressCtry' => 'nullable|alpha_dash',
          'tempAddressZip' => 'nullable|alpha_dash',
          'tempAddressCity' => 'nullable|alpha_dash',
          'tempAddressStreet' => 'nullable|alpha_dash',
          'tempAddressNum' => 'nullable|alpha_dash',
          'tempAddressStairDoor' => 'nullable|alpha_dash',
          'tempUOAddressCtry' => 'nullable|alpha_dash',
          'tempUOAddressZip' => 'nullable|alpha_dash',
          'tempUOAddressCity' => 'nullable|alpha_dash',
          'tempUOAddressStreet' => 'nullable|alpha_dash',
          'tempUOAddressNum' => 'nullable|alpha_dash',
          'tempUOAddressStairDoor' => 'nullable|alpha_dash',
          'selectCommuteFrom' => 'nullable|in:perm,temp,tempUO',
          'carAllowed' => 'nullable|in:0,1',
          'carAllowanceLimit' => 'nullable|numeric',
          'licensePlate' => 'nullable|alpha_dash',
          'cafeEligibleFrom' => 'nullable|date',
          'cafeEligibleUntil' => 'nullable|date',
          'cafeEligibleLimit' => 'numeric|nullable',
          'selectEmployeeType' => 'nullable|in:full_perm,full_cont,part_perm,part_cont,dual,student',
          'cafeSzepDining' => 'nullable|numeric',
          'cafeSzepLeisure' => 'nullable|numeric',
          'cafeSzepAccom' => 'nullable|numeric',
          'cafeDeclOpen' => 'nullable|in:0,1',
        ]);

        $km = session()->get('km');
        
        if($request['permAddressCity'] != null && $request['tempAddressStreet'] != null){
        $permAddr = $request['permAddressCtry'].
            '__'.$request['permAddressZip'].
            '__'.$request['permAddressCity'].
            '__'.$request['permAddressStreet'].
            '__'.$request['permAddressNum'].
            '__'.$request['permAddressStairDoor'] ;
        }
        else{
          $permAddr = null ;
        }
        if($request['tempAddressCity'] != null && $request['tempAddressStreet'] != null){
        $tempAddr = $request['tempAddressCtry'].
            '__'.$request['tempAddressZip'].
            '__'.$request['tempAddressCity'].
            '__'.$request['tempAddressStreet'].
            '__'.$request['tempAddressNum'].
            '__'.$request['tempAddressStairDoor'] ;
        }
        else{
          $tempAddr = null ;
        }
        if($request['tempUOAddressCity'] != null && $request['tempUOAddressStreet'] != null){
        $tempUOAddr = $request['tempUOAddressCtry'].
            '__'.$request['tempUOAddressZip'].
            '__'.$request['tempUOAddressCity'].
            '__'.$request['tempUOAddressStreet'].
            '__'.$request['tempUOAddressNum'].
            '__'.$request['tempUOAddressStairDoor'] ;
        }
        else{
          $tempUOAddr = null ;
        }

        $user = \App\User::find($id);

        //calculate eligibility and limit by starting date
        $eligstart = strtotime('+ 2 months',strtotime($request['joiningDate']));
        $eligstart = strtotime('+ 1 days',$eligstart);

        //calc limit
        if(date('Y',$eligstart) >= date('Y')){
          $diff = (mktime(0,0,0,12,31,date('Y',$eligstart)) - $eligstart) / 60/60/24 ;
          $lim = 384000 * ($diff/365);
        }
        else{
          $lim = 384000;
        }


        $user -> username =  $request['companyId'] != null ? $request['companyId'] : null ;
        $user -> email = $request['email'] != null ? $request['email'] : null ;
        $user -> fullname = $request['fullName'] != null ? encrypt($request['fullName']) : null ;
        $user -> joining_date = $request['joiningDate'] != null ? encrypt($request['joiningDate']) : null  ;
        $user -> termination_date = $request['terminationDate'] != null ? encrypt($request['terminationDate']) : null;
        $user -> user_status = 1 ;
        $user -> tax_id = $request['taxId'] != null ?  encrypt($request['taxId']) : null;
        $user -> perm_address = $permAddr != null ? encrypt($permAddr) : null ;
        $user -> temp_address = $tempAddr != null ? encrypt($tempAddr) : null ;
        $user -> temp_unoff_address = $tempUOAddr != null ? encrypt($tempUOAddr) : null ;
        $user -> daily_commute_from = $request['selectCommuteFrom'] != null ? $request['selectCommuteFrom'] : null;
        $user -> commute_distance = $km != null ? $km : null ;
        $user -> car_allowed =  $request['carAllowed'] == 1 ? '1' : '0' ;
        $user -> car_allowance_limit = $request['carAllowanceLimit'] != null ? $request['carAllowanceLimit'] : null;
        $user -> licence_plate = $request['licensePlate'] != null ? encrypt($request['licensePlate']) : null;
        $user -> preferred_lang = $request['selectPreferredLang'] != null ? $request['selectPreferredLang'] : null;
        $user -> roles = $request['selectUserRole'] != null ? encrypt($request['selectUserRole']) : null;
        $user -> cafe_emp_type = $request['selectEmployeeType'] != null ? encrypt($request['selectEmployeeType']) : null;
        $user -> cafe_eligible_from = date('Y-m-d',$eligstart);
        $user -> cafe_eligible_until = $request['terminationDate'] != null ? $request['terminationDate'] : null ;
        $user -> cafe_yearly_limit = round($lim,2) ;
        $user -> cafe_szep_dining_pocket = $request['cafeSzepDining'] != null ? encrypt($request['cafeSzepDining']) : null;
        $user -> cafe_szep_leisure_pocket = $request['cafeSzepLeisure'] != null ? encrypt($request['cafeSzepLeisure']) : null;
        $user -> cafe_szep_accom_pocket = $request['cafeSzepAccom'] != null ? encrypt($request['cafeSzepAccom']) : null;
        $user -> cafe_planning_open = $request['cafeDeclOpen'] == 1 ? '1' : '0' ;

        $user-> save();

        return redirect('userman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //abort_if(decrypt(auth()->user()->roles) != 'admin' , 403);

        \App\User::destroy($id);
        return redirect('userman');
    }
}
