@extends('layouts.homeLayout')
@section('content')

<div class="container mc-auto bg-white shadow-sm">
  <div class="container mc-auto bg-white shadow-sm">
    <h1 class="mc-auto">Cutoff Date MAnagement</h1>
  </div>
    @include('layouts.errordiv')
  <div class="container mc-auto bg-white shadow-sm">
    <form class="" action="{{ route('cutoffdates.store' ) }}" method="post">
      @csrf
      <table class="table .table-striped .table-hover" style="width:100%">
        <tr>
          <td></td>
          <td>{{__('Start')}}</td>
          <td>{{__('End')}}</td>
          <td></td>
        </tr>
        <tr>
          <td>{{__('Add New period:')}}</td>
          <td> <input type="date" name="coStart" /> </td>
          <td> <input type="date" name="coEnd" /> </td>
          <td> <button class="btn btn-primary" type="submit" name="button">Add this!</button> </td>
        </tr>
      </table>
    </form>
  </div>
  <div class="container mc-auto bg-white shadow-sm">
    <table class="table .table-striped .table-hover" style="width:100%">
      <thead>
        <tr>
          <th>{{ __('Year') }}</th>
          <th>{{__('Month') }}</th>
          <th>{{__('Starts on') }}</th>
          <th>{{__('Ends on') }}</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($cutoffDates as $date)
          <tr>
            <td>{{ $date-> year }}</td>
            <td>{{ $date-> month }}</td>
            <td>{{ $date-> start }}</td>
            <td>{{ $date-> end }}</td>
            <td>
              <form style="display:inline;" action="{{ route('cutoffdates.destroy', $date->id ) }}" method="post">
                {{ method_field('DELETE') }}
                @csrf
                <button type="submit" class="btn btn-primary btn-sm" >
                  <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>


@endsection
